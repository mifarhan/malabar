/**

 * @NApiVersion 2.x

 * @NScriptType ClientScript

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 13/06/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : ClientScript for Suitelet used for consolidating multiple invoices.  

 **************************************************************************************************************************************/
define(['N/currentRecord','N/https','N/url','N/format','N/ui/message','N/search','N/runtime'],

function(currentRecord,https,url,format,message,search,runtime) {
    var pId;
    function pageInit(scriptContext) {
        //alert('HIIII');
        var currentRecordObj = scriptContext.currentRecord;
        
        var submitButton = currentRecordObj.getField({
            fieldId: 'submitter'
        });
        
        var statusOfFileField = currentRecordObj.getField({
            fieldId: 'custpage_status_of_file'
        });
        statusOfFileField.isDisabled=true;

        var subsidiaryField = currentRecordObj.getField({
            fieldId: 'custpage_subsidiary'
        });
        subsidiaryField.isDisabled=true;
        var linecount = currentRecordObj.getLineCount({sublistId: 'custpage_invoices'});
        
        if(linecount > 0){
            submitButton.isDisabled = false;
        }else{
            submitButton.isDisabled = true;
        }
    }

    function fieldChanged(scriptContext){
        var currentRecordObj = currentRecord.get();
        if (scriptContext.fieldId == 'custpage_pageid') {
            var pageId = currentRecordObj.getValue({
                fieldId : 'custpage_pageid'
            });
            pageId = parseInt(pageId.split('_')[1]);
            pId=pageId;
            reloadPage();
        }
        if (scriptContext.fieldId == 'custpage_customer_name') {
            reloadPage();
        }

        if (scriptContext.fieldId == 'custpage_job') {
            reload();
        }
    }

    function saveRecord(context){
        var currentRecordObj = currentRecord.get();
        var totalSelected = [];
        var linecount = currentRecordObj.getLineCount({sublistId:'custpage_invoices'});
        if(linecount > 0){
            
            for(var i = 0; i < linecount; i++){
                var checked = currentRecordObj.getSublistValue({sublistId:'custpage_invoices',fieldId:'custpage_mark',line:i});
                var getId = currentRecordObj.getSublistValue({sublistId:'custpage_invoices',fieldId:'custpage_file',line:i});
                
                if(checked){
                    totalSelected.push(getId);
                }
            }
            if(totalSelected.length ==  1){
                alert ('Select More Than 1 Invoices');
                return false;
            }else if(totalSelected.length == 0){
                alert('Please select Invoices To Consolidate');
                return false;
            }else{
                var outputUrl = url.resolveScript({
                    scriptId:'customscript_softype_st_consolidate_invo',
                    deploymentId:'customdeploy_softype_st_consolidate_invo',
                    returnExternalUrl: true
                });
            }
        }
        return true;
    }

    function search(){
        reloadPage();
    }

    function reload(){
        var currentRecordObj = currentRecord.get();
        var businessTypeValue=currentRecordObj.getValue('custpage_business_type');
        var modeOfTransport=currentRecordObj.getValue('custpage_mode_of_transport');
        var customerType=currentRecordObj.getValue('custpage_customer_type');
        var customerName=currentRecordObj.getValue('custpage_customer_name');
        var jobId=currentRecordObj.getValue('custpage_job');

        if (jobId=='job') {
            reloadPage()
        }else{
            window.onbeforeunload = null;
            document.location=url.resolveScript({
                scriptId:'customscript_softype_st_consolidate_invo',
                deploymentId:'customdeploy_softype_st_consolidate_invo',
                params:{
                    'action':'getInvoices',
                    'bt':businessTypeValue,
                    'mot':modeOfTransport,
                    'ct':customerType,
                    'cn':customerName,
                    'job':jobId
                }
            });
        }
    }

    function reloadPage(){
        var currentRecordObj = currentRecord.get();
        var businessTypeValue=currentRecordObj.getValue('custpage_business_type');
        var modeOfTransport=currentRecordObj.getValue('custpage_mode_of_transport');
        var customerType=currentRecordObj.getValue('custpage_customer_type');
        var customerName=currentRecordObj.getValue('custpage_customer_name');

        /*if (businessTypeValue==0||modeOfTransport==0||customerType==0||customerName==0) {
            alert('Missing Value(s) For Required Field(s)');
        }else{
            if (pId!=null) {
                window.onbeforeunload = null;
                document.location=url.resolveScript({
                    scriptId:'customscript_softype_st_consolidate_invo',
                    deploymentId:'customdeploy_softype_st_consolidate_invo',
                    params:{
                        'action':'getInvoices',
                        'bt':businessTypeValue,
                        'mot':modeOfTransport,
                        'ct':customerType,
                        'cn':customerName,
                        'pid':pId
                    }
                });
            }else{
                window.onbeforeunload = null;
                document.location=url.resolveScript({
                    scriptId:'customscript_softype_st_consolidate_invo',
                    deploymentId:'customdeploy_softype_st_consolidate_invo',
                    params:{
                        'action':'getInvoices',
                        'bt':businessTypeValue,
                        'mot':modeOfTransport,
                        'ct':customerType,
                        'cn':customerName
                    }
                });
            } 
        }*/
        if (businessTypeValue==0||modeOfTransport==0||customerType==0||customerName==0) {
            alert('Missing Value(s) For Required Field(s)');
        }else{
            window.onbeforeunload = null;
            document.location=url.resolveScript({
                scriptId:'customscript_softype_st_consolidate_invo',
                deploymentId:'customdeploy_softype_st_consolidate_invo',
                params:{
                    'action':'getJobs',
                    'bt':businessTypeValue,
                    'mot':modeOfTransport,
                    'ct':customerType,
                    'cn':customerName
                }
            });
        }
    }

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        saveRecord: saveRecord,
        search:search
    };
});