/**
 *@NApiVersion 2.0
 *@NScriptType workflowactionscript
 */
/***************************************************************************************
 ** Copyright (c) 1998-2012 Softype, Inc.
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author :          AMAR GHORPADE / Rajendran Achari
 **@Version :         Revised version
 **@Description :     Workflow Action script For Custom Approval Matrix
 **@Update :  	      Added condition to handle Sales Order Approval Flow on 12 Sept 2018. 
					  Because SO has different status internal id other than PO, VB
 ***************************************************************************************/
var STD_APPROVAL_FIELD_ID = 'approvalstatus';
define(['N/record','N/search',"N/log"],	function(record,search,log){
	function WA_setApproval(context)
	{	var test = record.Type.PURCHASE_ORDER;
		var approved_custom = '1';
		var approved_stnd = '2';
		try
		{
			var recobj = context.newRecord;
			var currentRecordId = context.newRecord.id;
			log.emergency('currentRecordId',currentRecordId);
			var currentRecordType = context.newRecord.type;
			
			if(currentRecordType == 'salesorder')
			{
				STD_APPROVAL_FIELD_ID = 'orderstatus';
				approved_stnd = 'B';
			}
		log.emergency('context',JSON.stringify(context));
		if (context.type == 'edit') {
			return;
		}
    	log.debug('context.type context.type ==>',context.type);

			var orderstatus = recobj.getValue({fieldId: 'orderstatus'});
			var subsidiary = recobj.getValue({fieldId: 'subsidiary'});
			
			var location = recobj.getValue({fieldId: 'location'});
			var cost_center = recobj.getValue({fieldId: 'department'});
			var cust_form = recobj.getValue({fieldId: 'customform'});
			var field_class = recobj.getValue({fieldId: 'class'});
			var Cust_Approve_status = recobj.getValue({fieldId: 'custbody_approve_status'});
			//Product type Custom feild id
			// var Cust_Approve_status = recobj.getValue({fieldId: 'custbody_approve_status'});
			var RecordType_Internal_Id = getTransactionRecordTypeInternalId(currentRecordType);

			var tran_amount = recobj.getValue({fieldId: 'total'});
			var prod_type = recobj.getValue({fieldId: 'custbody_po_producttype'});

			log.debug('Transaction Created Details','currentRecordType '+currentRecordType+' currentRecordId '+currentRecordId+' tran_amount  '+tran_amount + ' prod_type' +prod_type + ' RecordType_Internal_Id ==>   '+RecordType_Internal_Id);
			//log.debug('Cust_Approve_status ==>',Cust_Approve_status);

			/* if(prod_type == '' || prod_type == null)
				return; */
			
			var MtrxCols = [];

			//columns
			MtrxCols.push(search.createColumn({
				name: 'custrecord_unistar_approvalmat_poststatu'
			}));
			MtrxCols.push(search.createColumn({
				name: 'custrecord_unistar_approvalmat_role'
			}));
			MtrxCols.push(search.createColumn({
				name: 'custrecord_unistar_approvalma_subsidiary'
			}));
			MtrxCols.push(search.createColumn({
				name: 'custrecord_unistar_approvalma_department'
			}));
			MtrxCols.push(search.createColumn({
				name: 'custrecord_unistar_approvalmat_location'
			}));
			MtrxCols.push(search.createColumn({
				name: 'custrecord_unistar_approvalmat_class'
			}));
			MtrxCols.push(search.createColumn({
				name: 'custrecord_unistar_approvalmat_prodtype'
			}));
log.debug('RecordType_Internal_Id ==>'+RecordType_Internal_Id+' Cust_Approve_status==>'+Cust_Approve_status+' tran_amount==>'+tran_amount+'prod_type==>'+prod_type);
			
			var Mtrxfilt = [];
			if(RecordType_Internal_Id == '15' || RecordType_Internal_Id == '6'){

				//Matrix filters
				Mtrxfilt.push(search.createFilter({
					name : 'isinactive',
					operator : search.Operator.IS,
					values :  'F'
				}));

				Mtrxfilt.push(search.createFilter({
					name: 'custrecord_unistar_approvalmat_trantype',
					operator: search.Operator.IS,
					values: RecordType_Internal_Id
				}));
				
				Mtrxfilt.push(search.createFilter({
					name: 'custrecord_unistar_approvalmat_prestatus',
					operator: search.Operator.IS,
					values: Cust_Approve_status
				}));
				Mtrxfilt.push(search.createFilter({
					name: 'custrecord_unistar_approvalmat_lowlimit',
					operator: search.Operator.LESSTHANOREQUALTO,
					values: tran_amount
				}));

				Mtrxfilt.push(search.createFilter({
					name: 'custrecord_unistar_approvalmat_uplimit',
					operator: search.Operator.GREATERTHANOREQUALTO,
					values: tran_amount
				}));
			} else if(RecordType_Internal_Id == '1'){
				Mtrxfilt.push(search.createFilter({
					name : 'isinactive',
					operator : search.Operator.IS,
					values :  'F'
				}));

				Mtrxfilt.push(search.createFilter({
					name: 'custrecord_unistar_approvalmat_trantype',
					operator: search.Operator.IS,
					values: RecordType_Internal_Id
				}));
				
				Mtrxfilt.push(search.createFilter({
					name: 'custrecord_unistar_approvalmat_prestatus',
					operator: search.Operator.IS,
					values: Cust_Approve_status
				}));
			}
			

			var found = false;
			var app_search_results = search.create({
				"type": "customrecord_unistar_approvalmatrix",
				"filters":Mtrxfilt,
				"columns":MtrxCols
			}).run(); // .getRange({start : 0, end : 1});

			var search_results = [];
			app_search_results.each(function(result) {
				search_results.push({
					id : result.id,
					custrecord_unistar_approvalmat_poststatu : result.getValue({ name: 'custrecord_unistar_approvalmat_poststatu'}),
					custrecord_unistar_approvalmat_role : result.getValue({ name: 'custrecord_unistar_approvalmat_role'}),
					custrecord_unistar_approvalma_subsidiary : result.getValue({ name: 'custrecord_unistar_approvalma_subsidiary'}),
					custrecord_unistar_approvalma_department : result.getValue({ name: 'custrecord_unistar_approvalma_department'}),
					custrecord_unistar_approvalmat_location : result.getValue({ name: 'custrecord_unistar_approvalmat_location'}),
					custrecord_unistar_approvalmat_class : result.getValue({ name: 'custrecord_unistar_approvalmat_class'}),
					custrecord_unistar_approvalmat_prodtype : result.getValue({ name: 'custrecord_unistar_approvalmat_prodtype'})
				});
				return true;
			});
			var search_length = search_results.length; 
			var search_matrix_id = '';
			var approval_role;
			var post_status;
			var additional_filter;
			var amount = 0;
			var flag = true;
			log.audit('search_length is '+search_length,search_results);
			
			if(search_length > 0)
			{
				var matrix_found = false;
				if(search_length == 1)
				{
					matrix_found = true;
					search_matrix_id = search_results[0].id;
					log.audit('search_matrix_id','Search Res Id '+search_matrix_id+' for transaction ID'+currentRecordId+' Type '+currentRecordType);
					post_status = search_results[0].custrecord_unistar_approvalmat_poststatu;
					log.audit('post_status',post_status);
					approval_role = search_results[0].custrecord_unistar_approvalmat_role;
					log.audit('approval_role',approval_role);
					additional_filter = search_results[0].custrecord_unistar_approvalmat_prodtype;
					log.audit('additional filter',additional_filter);										
					
				}else
				{
					for(var mtx = 0 ;mtx < search_length;mtx++)
					{
						var mtx_sub = search_results[mtx].custrecord_unistar_approvalma_subsidiary;
						var mtx_dept = search_results[mtx].custrecord_unistar_approvalma_department;
						var mtx_loc = search_results[mtx].custrecord_unistar_approvalmat_location;
						var mtx_class = search_results[mtx].custrecord_unistar_approvalmat_class;
						var mtx_prod_type = search_results[mtx].custrecord_unistar_approvalmat_prodtype;
						if( mtx_sub == subsidiary && mtx_dept == cost_center && mtx_loc == location && mtx_class == field_class && mtx_prod_type == prod_type)
						{
							found = true;
							log.audit('mutiple case search_results[mtx].id;',search_results[mtx].id+' for transaction ID '+currentRecordId+' Type '+currentRecordType);
							matrix_found = true;
							post_status = search_results[mtx].custrecord_unistar_approvalmat_poststatu;
							approval_role = search_results[mtx].custrecord_unistar_approvalmat_role;
							log.debug('approval_role',approval_role)
							
						}

					}
					
					if(!found)
					{
						log.debug('!found');
						for(var mt = 0 ;mt < search_length;mt++)
					{
						var mtx_sub = search_results[mt].custrecord_unistar_approvalma_subsidiary;
						
						var mtx_dept = search_results[mt].custrecord_unistar_approvalma_department;
						
						var mtx_loc = search_results[mt].custrecord_unistar_approvalmat_location;
						
						var mtx_class = search_results[mt].custrecord_unistar_approvalmat_class;
						
						var mtx_prod_type = search_results[mt].custrecord_unistar_approvalmat_prodtype;
					
					if(((mtx_sub == subsidiary) &&(mtx_sub != '')&&(subsidiary !=''))||((mtx_dept == cost_center) && (mtx_dept != '') && (cost_center != '')) ||( mtx_loc == location )|| ((mtx_class == field_class) && (mtx_class != '' )&& (field_class != ''))|| ((mtx_prod_type == prod_type )&& (mtx_prod_type != '') && (prod_type != ''))){
							log.debug('search_results[mt].id Exception case',search_results[mt].id+' for transaction ID'+currentRecordId+' Type '+currentRecordType);
							matrix_found = true;
							post_status = search_results[mt].custrecord_unistar_approvalmat_poststatu;
							approval_role = search_results[mt].custrecord_unistar_approvalmat_role;
							
							break;	
							
							
						}
						
						
					

					}	
					}
				}
						
				//ENSURING ATLEAST ONE APPROVAL MATRIX RECORD FOUND
				if(matrix_found)
				{
					log.debug('post_status',post_status);

					////////////////////////FOLLOWING CODE IF ACTION IS BEFORE SUBMIT//////////////////////////////
					if(post_status == approved_custom)
					{
						log.debug('inside if setting approved');
						log.debug('post_statuspost_status',post_status);
						log.debug('approval_roleapproval_role',approval_role);
						log.debug('approved_stndapproved_stnd',approved_stnd);

						//On Create Setting Value
						if(context.type == 'create'){
						 recobj.setValue({
							fieldId:'custbody_approve_status',
							value:post_status
						});
						recobj.setValue({
							fieldId:'custbody_approverrole',
							value:approval_role
						});
					
						//updated
						recobj.setValue({
							fieldId:STD_APPROVAL_FIELD_ID,
							value:approved_stnd
						});
					}

						if(context.type != 'create') {
							//updated
						log.debug('context.type != create');
						//bcz salesorder has different status internal id other than PO, VB 
						if(currentRecordType == 'salesorder'){
							
						recobj.setValue({
							fieldId:'custbody_approve_status',
							value:post_status
							});
						recobj.setValue({
							fieldId:'custbody_approverrole',
							value:approval_role
							});					
						recobj.setValue({
							fieldId:'orderstatus',
							value:approved_stnd
							});
							
						/* record.submitFields({
										type: context.newRecord.type,
										id: currentRecordId,
										values: {
											'custbody_approve_status': post_status,
											'custbody_approverrole':approval_role,
											'orderstatus':approved_stnd
										},
								options: {
									enableSourcing: false,
									ignoreMandatoryFields : true
								}
							}); */
						}else{
						
						recobj.setValue({
							fieldId:'custbody_approve_status',
							value:post_status
							});
						recobj.setValue({
							fieldId:'custbody_approverrole',
							value:approval_role
							});					
						recobj.setValue({
							fieldId:'approvalstatus',
							value:approved_stnd
							});		
							
/* 							record.submitFields({
										type: context.newRecord.type,
										id: currentRecordId,
										values: {
											'custbody_approve_status': post_status,
											'custbody_approverrole':approval_role,
											'approvalstatus':approved_stnd										
										},
								options: {
									enableSourcing: false,
									ignoreMandatoryFields : true
								}
							}); */
							log.debug('approvalstatus '+approved_stnd);
							log.debug('custbody_approve_status '+post_status);
							log.debug('custbody_approverrole '+approval_role);
						}
						
						}

					}else{
						log.debug('inside else '+currentRecordId);
						log.debug('inside else post_status '+post_status);
						log.debug('inside else approval_role '+approval_role);

						//On Create setting value
						if(context.type == 'create'){
							if(RecordType_Internal_Id == '6'){
								recobj.setValue({
									fieldId:'custbody_approve_status',
									value:post_status
								});
								recobj.setValue({
									fieldId:'custbody_approverrole',
									value:approval_role
								});
								recobj.setValue({
									fieldId:'custbody_po_producttype',
									value:additional_filter
								});
							}else {
								var employeeSearch = search.load({
									id: 'customsearch_employee_role_fm'
								});
								
								var searchResult2 = [];
								
								var empSearchResult2 = employeeSearch.run().getRange({
									start: 0,
									end: 100
								});
								
								for (var i = 0; i < empSearchResult2.length; i++) {
									var employeeName = empSearchResult2[i].getValue({
										name: 'internalid'
									});
									searchResult2.push(employeeName);				
								}					
								log.debug('Employee Name', searchResult2[0]);
								var next_approver2 = searchResult2[0];											
								log.debug('Next Approver Name', next_approver2);
								
								recobj.setValue({
									fieldId:'nextapprover',
									value:next_approver2
								});
								recobj.setValue({
									fieldId:'custbody_approve_status',
									value:post_status
								});
								recobj.setValue({
									fieldId:'custbody_approverrole',
									value:approval_role
								});
								recobj.setValue({
									fieldId:'custbody_po_producttype',
									value:additional_filter
								});
								
							}
						}
						log.debug('custbody_approve_status CREATE '+post_status);
						log.debug('custbody_approverrole CREATE '+approval_role);
						log.debug('custbody_po_producttype CREATE '+additional_filter);
						
						//Submit Record For Fields
						if(context.type != 'create'){
							if(RecordType_Internal_Id == '6'){
								record.submitFields({
									type: context.newRecord.type,
									id: currentRecordId,
									values: {
										'custbody_approve_status': post_status,
										'custbody_approverrole':approval_role,
										'custbody_po_producttype':additional_filter								
									},
									options: {
										enableSourcing: false,
										ignoreMandatoryFields : true
									}
								});
							}else {
								var empSearch = search.load({
									id: 'customsearch_employee_role'
								});
								var next_approver;
								var searchResult = [];
								
								var empSearchResult = empSearch.run().getRange({
									start: 0,
									end: 100
								});
								
								for (var i = 0; i < empSearchResult.length; i++) {
									var empName = empSearchResult[i].getValue({
										name: 'internalid'
									});
									searchResult.push(empName);				
								}					
								log.debug('Employee Name', searchResult[0]);
								
								next_approver = searchResult[0];						
								
								log.debug('Next Approver Name', next_approver);
								
								record.submitFields({
									type: context.newRecord.type,
									id: currentRecordId,
									values: {
										'nextapprover':next_approver,
										'custbody_approve_status': post_status,
										'custbody_approverrole':approval_role,
										'custbody_po_producttype':additional_filter								
									},
									options: {
										enableSourcing: false,
										ignoreMandatoryFields : true
									}
								});
							}
								log.debug('custbody_approve_status BUTTON '+post_status);
								log.debug('custbody_approverrole BUTTON '+approval_role);
								log.debug('custbody_po_producttype BUTTON '+additional_filter);
						}
					}
					///////////////////////////////////////////////////////
				}
			}

		}
		 catch(error)
		{
			// var htmlerror = '<p style = "color:red;">'+error.name+'</p>';
			// var recObj = record.submitFields({
			// type:context.newRecord.type,
			// id: context.newRecord.id,
			// values:{'custbody_dwz_error_catcher':htmlerror}
			// });
			log.debug('ERROR ON RECORD ',error);
		}
	}



function getRecData(recType){

		//[FIELD FOR GETTING TOTAL,RECTYPE ID]
		var data = ['','@NONE@'];
		if(recType== 'PURCHASEREQUISITION' || recType=='purchaserequisition')
			data = ['estimatedtotal',58];
		else if (recType== 'EXPENSEREPORT' || recType=='expensereport')
			data = ['amount',28];
		else if (recType== 'VENDORRETURNAUTHORIZATION' || recType=='vendorreturnauthorization')
			data = ['usertotal',43];
		else if (recType== 'TRANSFERORDER' || recType=='transferorder')
			data = ['total',48];
		else if (recType== 'SALESORDER' || recType=='salesorder')
			data = ['total',31];
		else if (recType== 'PURCHASEORDER' || recType=='purchaseorder')
			data = ['total',15];
		return data;
	}


function getTransactionRecordTypeInternalId(recordtype) {

		var internalId = '';
		var recordtype = recordtype.toLowerCase();
		switch(recordtype) {

		case 'journalentry':
			internalId = '1';
			break;

		case 'transfer':
			internalId = '2';
			break;

		case 'check':
			internalId = '3';
			break;

		case 'deposit':
			internalId = '4';
			break;

		case 'cashsale':
			internalId = '5';
			break;

		case 'estimate':
			internalId = '6';
			break;

		case 'invoice':
			internalId = '7';
			break;

		case '8':
			internalId = '';//Statement Charge
			break;

		case 'customerpayment':
			internalId = '9';
			break;

		case 'creditmemo':
			internalId = '10';
			break;

		case 'inventoryadjustment':
			internalId = '11';
			break;

		case 'inventorytransfer':
			internalId = '12';
			break;

		case '13':
			internalId = '';//Inventory Worksheet
			break;

		case '14':
			internalId = '';//Inventory Distribution
			break;

		case 'purchaseorder':
			internalId = '15';
			break;

		case 'itemreceipt':
			internalId = '16';
			break;

		case 'vendorbill':
			internalId = '17';
			break;

		case 'vendorpayment':
			internalId = '18';
			break;

		case '19':
			internalId = '';//Bill CCard
			break;

		case 'vendorcredit':
			internalId = '20';
			break;

		case '21':
			internalId = '';//Credit Card
			break;

		case 'creditcardrefund':
			internalId = '22';
			break;

		case '23':
			internalId = '';//Sales Tax Payment
			break;

		case 'paycheck':
			internalId = '24';
			break;

		case '25':
			internalId = '';//Payroll Liability Check
			break;

		case '26':
			internalId = '';//Payroll Adjustment
			break;

		case '27':
			internalId = '';//Liability Adjustment
			break;

		case 'expensereport':
			internalId = '28';
			break;

		case 'cashrefund':
			internalId = '29';
			break;

		case 'customerrefund':
			internalId = '30';
			break;

		case 'salesorder':
			internalId = '31';
			break;

		case 'itemfulfillment':
			internalId = '32';
			break;

		case 'returnauthorization':
			internalId = '33';
			break;

		case 'assemblybuild':
			internalId = '34';
			break;

		case 'assemblyunbuild':
			internalId = '35';
			break;

		case '36':
			internalId = '';//Currency Revaluation
			break;

		case 'opportunity':
			internalId = '37';
			break;

		case '38':
			internalId = '';//Commission
			break;

		case '39':
			internalId = '';//No Standard Transaction
			break;

		case 'customerdeposit':
			internalId = '40';
			break;

		case 'depositapplication':
			internalId = '41';
			break;

		case '42':
			internalId = '';//Bin Putaway Worksheet
			break;

		case 'vendorreturnauthorization':
			internalId = '43';
			break;

		case 'workorder':
			internalId = '44';
			break;

		case 'bintransfer':
			internalId = '45';
			break;

		case 'revenuecommitment':
			internalId = '46';
			break;

		case 'revenuecommitmentreversal':
			internalId = '47';
			break;

		case 'transferorder':
			internalId = '48';
			break;

		case '49':
			internalId = '';//Tegata Receivables
			break;

		case '50':
			internalId = '';//Tegata Payables
			break;

		case 'inventorycostrevaluation':
			internalId = '51';
			break;

		case '52':
			internalId = '';//Finance Charge
			break;

		case 'paycheckjournal':
			internalId = '56';
			break;

		case 'inventorycount':
			internalId = '57';
			break;

		}
		return internalId;

	}


	return {
		onAction: WA_setApproval,


	};
});