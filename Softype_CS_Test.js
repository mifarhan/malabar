/**
/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 20/08/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : This is Script is just to Enable/Disable fields According to the value 

 **	of the Interim Total Field of that particular Item Master.

 **	To Acheive this when user select an Item, on its post sourcing a lookupField is triggered which checks wheather the Interim Total Field

 ** of that Item Master Have any value or not.

 ** And if it has any value then all the custom fields of 'Item' sublist are enabled & all the standard fields are disabled.

 ** And if Interim Total Field does not have any value then all the custom fields are disabled and standard fields remain as it is.

 **************************************************************************************************************************************/

function pageInit(){
	//ar flagForScriptExecution=false;
	console.log('start');
}