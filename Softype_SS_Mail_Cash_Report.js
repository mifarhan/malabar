/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information"). 
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.                  
 **                       
 **@Author      :  Farhan Shaikh
 **@Dated       :  27/08/2019
 **@Version     :  1.0
 **@Description : Schedule Script to Mail Cash & Working Capital Report
 ***************************************************************************************/
define(['N/runtime', 'N/record','N/search', 'N/task','N/email'],

    function(runtime, record,search,task,email) {
        function execute(context){
            log.emergency('ScheduledScript')
            var availableCashAtBank,creditLineAvailable,availableFundsOnDeposit,debtorsPosition,wip,creditorsPosition,creditLineInUse,surplusWorkingCapital;
            var savedSearch=search.load('customsearch298').run().getRange({start:0,end:1000});
            log.emergency('savedSearch',savedSearch);

            for(var i=0;i<savedSearch.length;i++){

                var category=savedSearch[i].getText({
                   name: "custrecord_category",
                   summary: "GROUP"
                });
                category=category.trim();
                log.emergency('category',category);

                var balance=savedSearch[i].getValue({
                   name: "balance",
                   summary: "SUM"
                });
                log.emergency('balance',balance);
                balance=Number(balance)

                var formulaa=savedSearch[i].getValue({
                   name: "formulanumeric",
                   summary: "SUM",
                   formula: "{custrecord_creditline}+{balance}"
                });
                log.emergency('formulaa',formulaa);
                formulaa=Number(formulaa)

                if (category=='Current account') {
                    availableCashAtBank=balance;
                    //log.emergency('availableCashAtBank',availableCashAtBank);
                    //log.emergency('Current account');
                }else if(category=='Funds on deposit'){
                    availableFundsOnDeposit=balance
                    //log.emergency('Funds on deposit');
                }else if(category=='Over draft account'){
                    creditLineAvailable=formulaa
                    creditLineInUse=balance
                    //log.emergency('Over draft account');
                }else if(category=='Debtors'){
                    debtorsPosition=balance
                    //log.emergency('Debtors');
                }else if(category=='WIP'){
                    wip=balance
                    //log.emergency('WIP');
                }else if(category=='Creditors'){
                    creditorsPosition=balance
                    //log.emergency('Creditors');
                }
            }

            /*var creditLineAvailable=search.load('customsearch294').run().getRange({start:0,end:1000});
            log.emergency('creditLineAvailable',creditLineAvailable);

            var availableFundsOnDeposit=search.load('customsearch295').run().getRange({start:0,end:1000});
            log.emergency('availableFundsOnDeposit',availableFundsOnDeposit);

            var cashAtBank=search.load('customsearch296').run().getRange({start:0,end:1000});
            log.emergency('cashAtBank',cashAtBank);

            var tradeRec = search.lookupFields({
                type: 'account',
                id: 642,
                columns: ['balance']
            });

            var unbilledServiceInProgress = search.lookupFields({
                type: 'account',
                id: 2747,
                columns: ['balance']
            });

            var tradePay = search.lookupFields({
                type: 'account',
                id: 640,
                columns: ['balance']
            });

            var creditLineInUse=search.load('customsearch297').run().getRange({start:0,end:1000});
            log.emergency('creditLineInUse',creditLineInUse);*/

            var surplusWorkingCapital=availableCashAtBank+availableFundsOnDeposit+debtorsPosition+wip+creditorsPosition+creditLineInUse;
            surplusWorkingCapital=surplusWorkingCapital.toFixed(2);
            var senderField=search.lookupFields({
                type:'subsidiary',
                id:2,
                columns:['custrecord_emailfrom']
            })
            var senderId=senderField.custrecord_emailfrom[0].value;
            log.emergency('senderField',senderField);
            log.emergency('senderId',senderId);
            var emailList=search.lookupFields({
                type:'subsidiary',
                id:2,
                columns:['custrecord_emaillist']
            });
            log.emergency('emailList',emailList);
            log.emergency('custrecord_emaillist',emailList.custrecord_emaillist);
            emailList=emailList.custrecord_emaillist

            var emailArray=[];
            for(var i=0;i<emailList.length;i++){
                //value.push(emailList[i].value);
                var emaill=search.lookupFields({
                    type:'employee',
                    id:emailList[i].value,
                    columns:['email']
                });
                emailArray.push(emaill.email)
                //log.emergency('emailList',emailList);
            }
            log.emergency('emailArray',emailArray);

            email.send({
                author: senderId,
                recipients: emailArray,
                subject: 'Daily Bank & Cash Position Report',
                body: 'Dear Team,<br>Following is the Cash and working capital position as of now:<br><br><b>Cash Position : </b><br><br>&nbsp;&nbsp;&nbsp; Available Cash at bank : USD '+ availableCashAtBank+
                    '<br>&nbsp;&nbsp;&nbsp; Crédit line available : USD '+creditLineAvailable+
                    '<br>&nbsp;&nbsp;&nbsp; Available funds on deposit : USD '+availableFundsOnDeposit+
                    '<br><br><b> Working capital position (excl. cash) : </b><br><br>&nbsp;&nbsp;&nbsp; Debtors position : USD '+debtorsPosition+
                    '<br>&nbsp;&nbsp;&nbsp; WIP : USD '+wip+
                    '<br>&nbsp;&nbsp;&nbsp; Creditors Position : USD '+creditorsPosition+
                    '<br>&nbsp;&nbsp;&nbsp; Credit Line In Use : USD '+creditLineInUse+
                    '<br>&nbsp;&nbsp;&nbsp; Surplus Working Capital : USD '+surplusWorkingCapital
            });
            var date=getDate();
            log.emergency(date)
            
        }

        function getDate(){
            var date=new Date();
            var fullDate=date.getDate();
            fullDate+=' '+date.getMonth();
            fullDate+=' '+date.getYear();
            return fullDate;
        }

        function rescheduleCurrentScript() {
            var scheduledScriptTask = task.create({
                taskType: task.TaskType.SCHEDULED_SCRIPT
            });
            scheduledScriptTask.scriptId = runtime.getCurrentScript().id;
            scheduledScriptTask.deploymentId = runtime.getCurrentScript().deploymentId;
            return scheduledScriptTask.submit();
        }

        return {
            execute: execute
        };

    });