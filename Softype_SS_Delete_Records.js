/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                       
 **@Author      :  Rajendran C Achari
 **@Dated       :  11/13/2018 MM/DD/YYYY
 **@Version     :  1.0
 **@Description :  This script used to delete data from saved search.
 ***************************************************************************************/

function deleteRecords(type)
{
try{
	nlapiLogExecution('AUDIT', 'System', '***** Started *****');
	var ctx = nlapiGetContext();
	var stSavedSearchId = ctx.getSetting('script', 'custscript_saved_search_id');
	var stMaxNumToDelete = (ctx.getSetting('script', 'custscript_max_num_to_delete') > 400) ? 400 : ctx.getSetting('script', 'custscript_max_num_to_delete'); // to protect from exceeding governnce limits
	var stAutoReschedule = ctx.getSetting('script', 'custscript_auto_reschedule');
	var stDoDelete = ctx.getSetting('script', 'custscript_do_delete');
	var intMaxNumToDelete = parseInt(stMaxNumToDelete);
	var bReschedule = false;


	// - Find all Delete Record Saved Search results using getAllResults function. 
	var searchresults = getAllResults(null, stSavedSearchId);
	if ( searchresults != null ) 
	{
		var recType = searchresults[0].getRecordType();


		nlapiLogExecution('AUDIT', 'System', 'Found ' + searchresults.length + ' ' + recType + 's');
		var intNumToDelete = searchresults.length;
		if (intNumToDelete > intMaxNumToDelete)
		{
			intNumToDelete = intMaxNumToDelete;
			bReschedule = true;
		}
		for ( var i=0; i<intNumToDelete;i++) 
		{
			if (searchresults != null ) 
			{
			  	try 
				{
					var searchresult = searchresults[i];
					var recID = searchresult.getId();
					var recType = searchresult.getRecordType();
					nlapiLogExecution('DEBUG', recType + ' ID: ' + recID, '(' + (i + 1) + ' of ' + intNumToDelete + ') ' + stDoDelete + ' - Deleting');
					if (stDoDelete == 'T')
					{
						nlapiDeleteRecord(recType, recID);
					}
			  	}
			  	catch (e) 
    			  	{
        				if (e.getDetails != undefined)
        				{
            					nlapiLogExecution('ERROR', recType + ' ID: ' + recID, 'Processing Error: <br>'+e.getCode() + '<br>' + e.getDetails());    
        				}
        				else 
        				{
            					nlapiLogExecution('ERROR', recType + ' ID: ' + recID, 'Processing Error: <br>'+e.toString());                            
        				}
    			  	}

			}
		}
	} 
	else 
	{
		nlapiLogExecution('AUDIT', 'System', 'No ' + stRecordType + 's found by search');
	}
	if (bReschedule && stAutoReschedule == 'T')
	{
		nlapiLogExecution('DEBUG', 'System', 'ScriptID: ' + ctx.getScriptId() + ' DeployID: ' + ctx.getDeploymentId());
		nlapiLogExecution('DEBUG', 'System', 'Rescheduled: ' + nlapiScheduleScript(ctx.getScriptId(), ctx.getDeploymentId()));
	}
	nlapiLogExecution('AUDIT', 'System', '***** Finished *****');
} 
catch(e){
	nlapiLogExecution('ERROR',e.toString()); 
}	
	
}

function getAllResults(stRecordType,stSavedSearch)
{
	var stLoggerTitle = "getAllResults";	
	var arrResults = new Array();
	var searchIndex = 0;
	do
	{        
		var columns = 	
			[
			  new nlobjSearchColumn('internalid').setSort()
			];

		var filters = 	
			[
			  new nlobjSearchFilter ('internalidnumber', null, 'greaterthan', searchIndex)
			];

		
		var results = nlapiSearchRecord(stRecordType, stSavedSearch, filters, columns);

		if(results)
		{
			searchIndex = results[results.length-1].getId();
			arrResults = arrResults.concat(results);            
		}
	}
	while(results && results.length == 1000);	
	return arrResults;
}