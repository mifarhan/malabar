/**

 * @NApiVersion 2.x

 * @NScriptType UserEventScript

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 12/06/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : UserEvent Script on Sales Order (Job) to set the Transaction Status.  

 **************************************************************************************************************************************/
define(['N/record', 'N/runtime', 'N/search', 'N/format'],

    function(record, runtime, search, format) {
	
        function beforeSubmit(scriptContext) {

        }



        /**

         * Function definition to be triggered after record is submitted.

         *

         * @param {Object} scriptContext

         * @param {Record} scriptContext.newRecord - New record

         * @param {Record} scriptContext.oldRecord - Old record

         * @param {string} scriptContext.type - Trigger type

         * @Since 2015.2

         */

        function afterSubmit(scriptContext) {
            if (scriptContext.type == scriptContext.UserEventType.DELETE) {
                return
            }
            log.audit('executionContext.type', runtime.executionContext);
            log.debug('scriptContext.type', scriptContext.type);

            if (scriptContext.type == scriptContext.UserEventType.CREATE || scriptContext.type == scriptContext.UserEventType.EDIT) {
            	
            	log.emergency('inside main if');

                /* Getting the current record object*/
                var currentRecordObj = scriptContext.newRecord;
                log.debug('CurrRecObj', currentRecordObj);

                /* Get all the values from the Transaction record*/
                var recType = currentRecordObj.type;
                log.debug('Rec Type', recType);
                /* Get the Job's Internal ID*/
                var jobId = currentRecordObj.id;
                log.debug('Job id', jobId);

                /* Get the value of current transaction status*/
                var transactionStatus = currentRecordObj.getValue('custbody_transactionstatus');
                log.debug('Transaction Status', transactionStatus);

                /* Get the value for Business Type*/
                var businessType = currentRecordObj.getValue('custbody_businesstype');
                log.debug('Business Type', businessType);

                /* Get the value for Mode Of Transport*/
                var modeOfTransport = currentRecordObj.getValue('custbody_modeoftransport');
                log.debug('Mode Of Transport', modeOfTransport);

                /* Get the value for Subsidiary*/
                var subsidiary = currentRecordObj.getValue('subsidiary');
                log.debug('Subsidiary', subsidiary);

                /* Get the value for Customer Type*/
                var customerType = currentRecordObj.getValue('custbodycustbody_customertype');
                log.debug('Customer Type', customerType);

                var serviceList = currentRecordObj.getValue('custbody_servicetype');
                log.debug('Service List', serviceList);

                var triggerFilter = new Array();

                /*Filter Out the Records of Status Matrix based on the different field's value of SalesOrder(Job)*/
                /*Will use filter only if it has non-empty value*/
                if (recType!= null && recType != '' && recType != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_transactiontype',
                        operator: 'is',
                        values: recType
                    }));
                }
                
                if (businessType!= null && businessType != '' && businessType != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_businesstype',
                        operator: 'is',
                        values: businessType
                    }));
                }

                if (modeOfTransport!= null && modeOfTransport != '' && modeOfTransport != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_modeoftransport',
                        operator: 'is',
                        values: modeOfTransport
                    }));
                }

                if (transactionStatus!= null && transactionStatus != '' && transactionStatus != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_previousstatus',
                        operator: 'is',
                        values: transactionStatus
                    }));
                }  

                if (subsidiary!= null && subsidiary != '' && subsidiary != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_subsidiary',
                        operator: 'is',
                        values: subsidiary
                    }));
                }

                if (businessType!= null && businessType != '' && businessType != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_customertype',
                        operator: 'is',
                        values: customerType
                    }));
                }


                if (serviceList!= null && serviceList != '' && serviceList != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_servicelist',
                        operator: 'anyof',
                        values: serviceList
                    }));
                }

                var triggerColumns =new Array();

                /*Used two columns to set the Transaction Status according to previous values*/
                triggerColumns.push(search.createColumn({name:'internalid'}));
                triggerColumns.push(search.createColumn({name:'custrecord_matrix_transactionstatus'}));
                triggerColumns.push(search.createColumn({name:'custrecord_matrix_servicelist'}));

                
                var searchStatusTrigger = search.create({
                    type: 'customrecord_statusmatrix',
                    filters: triggerFilter,
                    columns: triggerColumns
                }).run().getRange({start:0,end:1000});

                log.debug('Search Result',searchStatusTrigger);

                /*Setting the value for Transaction Status Field*/
                var transStatus=searchStatusTrigger[0].getValue('custrecord_matrix_transactionstatus');
                var searchResultServiceList=searchStatusTrigger[0].getValue('custrecord_matrix_servicelist');
                log.debug('Search Result [Service List]',searchResultServiceList);


                record.submitFields({
                    type: 'salesorder',
                    id: jobId,
                    values: {
                        'custbody_transactionstatus': transStatus
                    }
                });
                //currentRecordObj.setValue('custbody_transactionstatus',searchStatusTrigger.getValue('custrecord_matrix_transactionstatus'));
            }
        }

        return {
            afterSubmit: afterSubmit
        };
});