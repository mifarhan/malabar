/**

 * @NApiVersion 2.x

 * @NScriptType UserEventScript

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 16/07/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : UserEvent Script on Invoice to set the Customized Invoice Number For Consolidated Invoice.  

 **************************************************************************************************************************************/
define(['N/record', 'N/runtime', 'N/search', 'N/format'],

    function(record, runtime, search, format) {

        /**
         * Function definition to be triggered after record is submitted.
         *
         */

        function beforeSubmit(scriptContext) {
            log.emergency('Auto Numbering Script Triggered',JSON.stringify(scriptContext));
        	if (scriptContext.type != scriptContext.UserEventType.CREATE) {
        		return;
        	}
            var newRec = scriptContext.newRecord;
            var customForm=newRec.getValue('customform')
            if (customForm==114) {
                var searchFilter =new Array();
                var searchColumns =new Array();

            	searchFilter.push(search.createFilter({
                    name: 'custrecord_invoiceformlist',
                    operator: search.Operator.IS,
                    values: customForm
                }));

                searchFilter.push(search.createFilter({
                    name: 'isinactive',
                    operator: search.Operator.IS,
                    values: false
                }));

                searchColumns.push(search.createColumn('custrecord_counter'));
                searchColumns.push(search.createColumn('custrecord_prefix'));

                var searchedCounterRecord = search.create({
                    type: 'customrecord_invoicecounter',
                    filters: searchFilter,
                    columns: searchColumns
                }).run().getRange({start:0,end:1000});

                log.emergency('searchedCounterRecord',searchedCounterRecord);

                for(var i=0;i<searchedCounterRecord.length && searchedCounterRecord!=null;i++){
                    var preFix=searchedCounterRecord[i].getValue('custrecord_prefix');
                    var count=searchedCounterRecord[i].getValue('custrecord_counter');
                    count=Number(count)+1;
                    var newInvoiceNumber=preFix+'-'+count;

                    record.submitFields({
                        type: 'customrecord_invoicecounter',
                        id: searchedCounterRecord[i].id,
                        values: {
                            'custrecord_counter': count
                        }
                    });
                    newRec.setValue('tranid', newInvoiceNumber);

                }
            }
        }

        return {
            beforeSubmit: beforeSubmit
        };
});