/**
 *@NApiVersion 2.0
 *@NScriptType workflowactionscript
 */
/***************************************************************************************
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author  : Farhan Shaikh
 **@Dated   : 13/06/2019   DD/MM/YYYY
 **@Version :         1.0
 **@Description :     Workflow Action script For Creating Files 
 ***************************************************************************************/
define(['N/record','N/search',"N/log","N/url","N/redirect"],    function(record,search,log,url,redirect){
    function createFile(context){
        var currentRecordId = context.newRecord.id;
        var soRecObj = context.newRecord;
        var businessType=soRecObj.getValue('custbody_businesstype')
        log.emergency('businessType',businessType);
        var fileFilters = new Array();
        var fileColumns = new Array();

        fileFilters.push(search.createFilter({
            name: 'createdfrom',
            operator: 'is',
            values: currentRecordId
        }));

        var invoiceSearch = search.create({
            type: 'invoice',
            filters: fileFilters
        }).run().getRange(0, 1000);
        log.emergency('invoice',invoiceSearch);

        if (invoiceSearch==0) {
            var transformObj = record.transform({
             fromType: 'salesorder',
             fromId: currentRecordId,
             toType: 'invoice',
             isDynamic: true,
            });

            //transformObj.setValue('custbody_mcafilenum','WAFileCreated');

            var transformedInv=transformObj.save();
            log.emergency('Transformed Invoice Created',transformedInv);

            var output = url.resolveRecord({
             recordType: 'invoice',
             recordId: transformedInv,
             isEditMode: true
            });
    
            log.emergency('output',output);
    
            redirect.toRecord({
             type : 'invoice',
             id : transformedInv,
             //parameters: {'e':'T'}
            });
        }else{
            log.emergency('invoice to copy',invoiceSearch[0].id);

    
            var fieldFilters = new Array();
            var fieldCoulmns = new Array();
    
            fieldFilters.push(search.createFilter({
                name: 'custrecord_createfield_businesstype',
                operator: 'is',
                values: businessType
            }));
    
            fieldCoulmns.push(search.createColumn({
                name:'custrecord_createfield_internalid'
            }));
    
            var fieldsSearch = search.create({
                type: 'customrecord_createafile',
                filters: fieldFilters,
                columns: fieldCoulmns
            }).run().getRange(0, 1000);
            log.emergency('invoice',fieldsSearch);
    
            log.emergency('invoice to copy',fieldsSearch[0].id);
    
            var fieldsToBlankArray= new Array();
    
            var fieldsToBlank=fieldsSearch[0].getText('custrecord_createfield_internalid');
            log.emergency('fieldsToBlank',fieldsToBlank);
            fieldsToBlank=fieldsToBlank.split(',');
            fieldsToBlank.push('custbody_fieldforscript') 
    
            log.emergency('jsonObj',fieldsToBlankArray);
            var objRecord = record.copy({
             type: 'invoice',
             id: invoiceSearch[0].id,
             isDynamic: true
            });
    
            for(var i=0;i<fieldsToBlank.length;i++){
                var keyy = fieldsToBlank[i];
                //log.emergency('keyy',keyy);
                /*var jsonObj = {keyy :''}*/
                var fieldType=objRecord.getField(keyy).type;
                log.emergency('fieldType',fieldType);
                if (fieldType!='checkbox') {
                    objRecord.setValue(keyy,'')
                }else{
                    objRecord.setValue(keyy,false);
                }
                
            }
            
            //objRecord.setValue('custbody_mcafilenum','createFileButton');
            var copiedInvoice=objRecord.save();
            log.emergency('copiedInvoice',copiedInvoice);
    
            var output = url.resolveRecord({
             recordType: 'invoice',
             recordId: copiedInvoice,
             isEditMode: true
            });
    
            log.emergency('output',output);
    
            redirect.toRecord({
             type : 'invoice',
             id : copiedInvoice,
             //parameters: {'e':'T'}
            });
        }
        



        /*log.emergency('currentRecordId');
        var recobj = context.newRecord;
        var currentRecordId = context.newRecord.id;
        log.emergency('currentRecordId',currentRecordId);
        var currentRecordType = context.newRecord.type;
        log.emergency('currentRecordType',currentRecordType);

        var invoiceObj=record.create({
            type:'invoice',
            isDynamic:true
        });

        var customer=recobj.getValue('entity');
        invoiceObj.setValue('entity',customer)
        //var subsidiary=recobj.getValue('subsidiary');
        //var classs=recobj.getValue('class');
        var department=recobj.getValue('department');
        invoiceObj.setValue('department',department);

        var location=recobj.getValue('location');
        invoiceObj.setValue('location',location);

        invoiceObj.setValue('custbody_customexchangerate',1);
        invoiceObj.setValue('custbody_standardexchangerate',1);
        invoiceObj.setValue('createdfrom',currentRecordId);
        invoiceObj.setValue('custbody_customerlistoninvoice',customer);

        var numLines = recobj.getLineCount({
         sublistId: 'item'
        });

        log.emergency('numLines',numLines);

        for(var i=0;i<numLines;i++){
            var sublistItemValue = recobj.getSublistValue({
             sublistId: 'item',
             fieldId: 'item',
             line: i
            });

            var sublistQuantityValue = recobj.getSublistValue({
             sublistId: 'item',
             fieldId: 'quantity',
             line: i
            });

            var sublistRateValue = recobj.getSublistValue({
             sublistId: 'item',
             fieldId: 'rate',
             line: i
            });

            var sublistTaxCValue = recobj.getSublistValue({
             sublistId: 'item',
             fieldId: 'taxcode',
             line: i
            });

            invoiceObj.selectNewLine({ 
                sublistId: 'item'      
            });

            invoiceObj.setCurrentSublistValue({
             sublistId: 'item',
             fieldId: 'item',
             value: sublistItemValue
            });

            invoiceObj.setCurrentSublistValue({
             sublistId: 'item',
             fieldId: 'quantity',
             value: sublistQuantityValue
            });

            invoiceObj.setCurrentSublistValue({
             sublistId: 'item',
             fieldId: 'rate',
             value: sublistRateValue
            });

            invoiceObj.setCurrentSublistValue({
             sublistId: 'item',
             fieldId: 'taxcode',
             value: sublistTaxCValue
            });

            invoiceObj.commitLine({ 
                sublistId: 'item'
            });
        }

        var invoiceId= invoiceObj.save();

        if (invoiceId) {
            log.emergency('invoiceId',invoiceId);
        }

        log.emergency('entity',customer);*/
    }

    return {
        onAction: createFile
    };
});