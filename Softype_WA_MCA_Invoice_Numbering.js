/**
 *@NApiVersion 2.0
 *@NScriptType workflowactionscript
 */
/***************************************************************************************
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author  : Farhan Shaikh
 **@Dated   : 09/09/2019   DD/MM/YYYY
 **@Version :         1.0
 **@Description :     Workflow Action script For Setting Customized Invoice Number On a particular field
 ***************************************************************************************/
define(['N/record','N/search',"N/log","N/url","N/redirect"],    function(record,search,log,url,redirect){
    function createFile(context){
        var currentRecordId = context.newRecord.id;
        var fileRecObj = context.newRecord;
        var customForm=fileRecObj.getValue('customform')

        var searchFilter =new Array();
        var searchColumns =new Array();

        searchFilter.push(search.createFilter({
            name: 'custrecord_fileinvoiceformlist',
            operator: search.Operator.IS,
            values: customForm
        }));
        searchFilter.push(search.createFilter({
            name: 'isinactive',
            operator: search.Operator.IS,
            values: false
        }));

        searchColumns.push(search.createColumn('custrecord_mcainvoicecounter'));
        searchColumns.push(search.createColumn('custrecord_mcainvoiceprefix'));
        searchColumns.push(search.createColumn('custrecord_fileinvoiceformlist'));
        var searchedCounterRecord = search.create({
            type: 'customrecord_invoicecounter',
            filters: searchFilter,
            columns: searchColumns
        }).run().getRange({start:0,end:1000});
        log.emergency('searchedCounterRecord',searchedCounterRecord);
        for(var i=0;i<searchedCounterRecord.length && searchedCounterRecord!=null;i++){
            var preFix=searchedCounterRecord[i].getValue('custrecord_mcainvoiceprefix');
            var count=searchedCounterRecord[i].getValue('custrecord_mcainvoicecounter');
            count=Number(count)+1;
            var newInvoiceNumber=preFix+''+count;
            record.submitFields({
                type: 'customrecord_invoicecounter',
                id: searchedCounterRecord[i].id,
                values: {
                    'custrecord_mcainvoicecounter': count
                }
            });
            /*record.submitFields({
                type: 'invoice',
                id: currentRecordId,
                values: {
                    'custbody_mcainvoicenum': newInvoiceNumber
                }
            });*/
            fileRecObj.setValue('custbody_mcainvoicenum', newInvoiceNumber);
        }
        
    }

    return {
        onAction: createFile
    };
});