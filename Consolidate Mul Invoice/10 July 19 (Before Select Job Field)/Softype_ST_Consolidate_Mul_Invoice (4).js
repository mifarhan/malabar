/**

 * @NApiVersion 2.x

 * @NScriptType Suitelet

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 13/06/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : Suitelet for consolidating multiple invoices.  

 **************************************************************************************************************************************/
 define(['N/ui/serverWidget', 'N/search', 'N/record', 'N/url', 'N/https', 'N/runtime', 'N/format', 'N/error'],
    function(serverWidget, search, record, url, https, runtime, format, error) {
        function onRequest(context) {

            var action = context.request.parameters.action;
            log.audit('Context', JSON.stringify(context));
            log.audit('Action', action);
            var recordIds = [];
            log.debug('Start');



            if (context.request.method === 'GET') {

                var businessTypeParam=context.request.parameters.bt;
                var modeOfTransportParam=context.request.parameters.mot;
                var customerTypeParam=context.request.parameters.ct;
                var customerNameParam=context.request.parameters.cn;            

                var form = serverWidget.createForm({
                    title: 'Consolidate Invoices'
                });
                var fileFilters = new Array();
                var fileColumns = new Array();

                fileFilters.push(search.createFilter({
                    name: 'name',
                    operator: 'is',
                    values: 'Softype_CS_Consolidate_Mul_Invoice.js'
                }));

                var clientSearch = search.create({
                    type: 'file',
                    filters: fileFilters
                }).run().getRange(0, 1000);
                log.debug('FileSearch', clientSearch);

                form.clientScriptFileId = clientSearch[0].id;

                var businessType = form.addField({
                    id: 'custpage_business_type',
                    type: serverWidget.FieldType.SELECT,
                    label: 'Business Type',
                    source:'customlist_businesstype'
                });

                businessType.isMandatory = true;

                businessType.layoutType = serverWidget.FieldLayoutType.STARTROW;
                businessType.updateBreakType({
                    breakType: serverWidget.FieldBreakType.STARTROW
                });

                if (businessTypeParam) {
                    businessType.defaultValue=businessTypeParam;
                }

                var modeOfTransport = form.addField({
                    id: 'custpage_mode_of_transport',
                    type: serverWidget.FieldType.SELECT,
                    label: 'Mode of Transport',
                    source:'customlist_modeoftransport'
                });

                modeOfTransport.isMandatory = true;
                if (modeOfTransportParam) {
                    modeOfTransport.defaultValue=modeOfTransportParam;
                }

                log.audit('Mode of Transport', modeOfTransportParam);

                var customerType = form.addField({
                    id: 'custpage_customer_type',
                    type: serverWidget.FieldType.SELECT,
                    label: 'Customer Type',
                    source: 'customlist_customertype'
                });
                customerType.isMandatory = true;

                if (customerTypeParam) {
                    customerType.defaultValue=customerTypeParam;
                }

                var customerName = form.addField({
                    id: 'custpage_customer_name',
                    type: serverWidget.FieldType.SELECT,
                    label: 'Customer Name',
                    source:'customer'
                });
                customerName.isMandatory = true;

                if (customerNameParam) {
                    customerName.defaultValue=customerNameParam;
                }

                var subsidiaryField = form.addField({
                    id: 'custpage_subsidiary',
                    type: serverWidget.FieldType.TEXT,
                    label: 'Subsidiary',
                });
                if (true) {
                    subsidiaryField.defaultValue = 'MCA';
                }
                var statusOfFile = form.addField({
                    id: 'custpage_status_of_file',
                    type: serverWidget.FieldType.TEXT,
                    label: 'Status of FILE',
                });

                if (true) {
                    statusOfFile.defaultValue = 'APPROVED';
                }

                var selectOptions = form.addField({
                    id: 'custpage_pageid',
                    label: 'Page Index',
                    type: serverWidget.FieldType.SELECT
                });

                var submitButton = form.addSubmitButton({
                    label: 'Create'
                });

                form.addButton({
                    id: 'custpage_search',
                    label: 'Search',
                    functionName: 'search();'
                });

                if (action=='getInvoices') {
                    submitButton.isDisabled = false;
                    var sublistObj = form.addSublist({
                        id: 'custpage_invoices',
                        type: serverWidget.SublistType.LIST,
                        label: 'Invoices'
                    });

                    //log.debug('isChecked', isChecked);
                    //if (isChecked != 'true') {

                    sublistObj.addMarkAllButtons();

                    var markBox = sublistObj.addField({
                        id: 'custpage_mark',
                        type: serverWidget.FieldType.CHECKBOX,
                        label: 'Select'
                    });

                    sublistObj.addField({
                        id: 'custpage_file',
                        type: serverWidget.FieldType.TEXT,
                        label: 'File'
                    });

                    sublistObj.addField({
                        id: 'custpage_creation_date',
                        type: serverWidget.FieldType.TEXT,
                        label: 'Creation Date'
                    });
                    sublistObj.addField({
                        id: 'custpage_services',
                        type: serverWidget.FieldType.TEXT,
                        label: 'Services'
                    });

                    sublistObj.addField({
                        id: 'custpage_invoice_iid',
                        type: serverWidget.FieldType.TEXT,
                        label: 'Internal Id'
                    });

                    var PAGE_SIZE=20;
                    var invoices=getInvoices(PAGE_SIZE,businessTypeParam,modeOfTransportParam,customerNameParam,customerTypeParam);
                    log.emergency('Searched Invoices', invoices);
                    log.emergency('Searched Invoices', invoices.count);

                    if (invoices) {
                        var pageCount = parseInt(invoices.count / PAGE_SIZE);
                        log.emergency('pageCount',pageCount)
                    }
                    var pageId=context.request.parameters.pid;
                    if (!pageId) {
                        pageId=0;
                    }
                    if (invoices.count != 0) {
                        for (i = 0; i < pageCount; i++) {
                            if (i == pageId) {
                                selectOptions.addSelectOption({
                                    value: 'pageid_' + i,
                                    text: ((i*PAGE_SIZE) + 1) + ' - ' + ((i + 1)*PAGE_SIZE),
                                    isSelected: true
                                });
                            } else {
                                selectOptions.addSelectOption({
                                    value: 'pageid_' + i,
                                    text: ((i*PAGE_SIZE) + 1) + ' - ' + ((i + 1)*PAGE_SIZE)
                                });
                            }
                        }
                        var page = invoices.fetch({ index: pageId });
                        var i=0;
                        page.data.forEach(function (result) {
                            log.emergency('foeEach Result ', result);
                            var invoiceCreated = result;
                            log.emergency('Invoice ID', invoiceCreated);
    
                            sublistObj.setSublistValue({
                             sublistId: 'custpage_invoices',
                             id: 'custpage_file',
                             line: i,
                             value: result.getValue('tranid')
                            });
    
                            sublistObj.setSublistValue({
                             sublistId: 'custpage_invoices',
                             id: 'custpage_creation_date',
                             line: i,
                             value: result.getValue('trandate')
                            });
                            var services=result.getText('custbody_servicetype');
                            log.emergency('Services',services);
                            log.emergency('Services',services.length);     
                            if (services.length==0 || services == null || services == '') {
                                sublistObj.setSublistValue({
                                 sublistId: 'custpage_invoices',
                                 id: 'custpage_services',
                                 line: i,
                                 value: ' '
                                });
                            }else{
                                var serviceString='';
                                sublistObj.setSublistValue({
                                 sublistId: 'custpage_invoices',
                                 id: 'custpage_services',
                                 line: i,
                                 value: services
                                });
                            }
    
                            sublistObj.setSublistValue({
                             sublistId: 'custpage_invoices',
                             id: 'custpage_invoice_iid',
                             line: i,
                             value: result.id
                            });
                            i++;
                        });
                    }else{
                        sublistObj.setSublistValue({
                         sublistId: 'custpage_invoices',
                         id: 'custpage_file',
                         line: 0,
                         value: 'Files Not Found'
                        });

                        markBox.updateDisplayType({
                            displayType: serverWidget.FieldDisplayType.DISABLED
                        });
                    }
                    context.response.writePage(form);
                }
                var scriptObj = runtime.getCurrentScript();
                log.audit("Remaining Usage", scriptObj.getRemainingUsage());

                context.response.writePage(form);
            }else{
                var delimiter = /\u0002/;
                var delimiter1 = /\u0001/;
                var sublistData = context.request.parameters.custpage_invoicesdata.split(delimiter);
                log.audit('SUBLIST DATA', sublistData.length);
                log.audit('SUBLIST JSON', JSON.stringify(sublistData))
                var sublistArray = sublistData[0].split(delimiter1);
                log.audit('Subslist data array', sublistArray[0]);

                var sublistLength = sublistData.length;
                var invoicesInternalIds = new Array();
                for (var i = 0; i < sublistLength; i++) {
                    var sublistArray = sublistData[i].split(delimiter1);
                    var checked = sublistArray[0];
                    if (checked == 'T') {
                        var invoiceIid = sublistArray[4];
                        invoicesInternalIds.push(invoiceIid);
                    }
                }

                log.audit('Internal IDs of Selected Invoices',invoicesInternalIds);

                var invoiceRecord = record.create({
                    type: 'invoice',
                    isDynamic:true
                });

                for(var i=0;i<invoicesInternalIds.length;i++){
                    
                    if (i==0) {
                        var invoiceObj = record.load({
                            type: 'invoice',
                            id: invoicesInternalIds[i],
                            isDynamic: true
                        });
                        log.audit('Selected Invoice[Primary]',invoiceObj);

                        var customer=invoiceObj.getValue('entity');
                        log.audit('Customer',customer);

                        var customerType=invoiceObj.getValue('custbodycustbody_customertype');
                        log.audit('Customer Type',customerType);

                        var modeOfTransport=invoiceObj.getValue('custbody_modeoftransport');
                        log.audit('Mode Of Transport',modeOfTransport);

                        var serviceType=invoiceObj.getValue('custbody_servicetype');
                        log.audit('Service Type',serviceType);

                        var businessType=invoiceObj.getValue('custbody_businesstype');
                        log.audit('Business Type',businessType);

                        var subsidiary=invoiceObj.getValue('subsidiary');
                        log.audit('subsidiary',subsidiary);

                        var importExportLicense=invoiceObj.getValue('custbody_importexportlicense');
                        log.audit('importExportLicense',importExportLicense);

                        var classificationClass=invoiceObj.getValue('class');
                        log.audit('class',classificationClass);

                        var tranDate=invoiceObj.getValue('trandate');
                        log.audit('tranDate',tranDate);

                        var postingPeriod=invoiceObj.getValue('postingperiod');
                        log.audit('postingPeriod',postingPeriod);

                        var commercialInvoiceNumber=invoiceObj.getValue('otherrefnum');
                        log.audit('commercial Invoice Number',commercialInvoiceNumber);

                        var opportunity=invoiceObj.getValue('opportunity');
                        log.audit('opportunity',opportunity);

                        var department=invoiceObj.getValue('department');
                        log.audit('department',department);

                        var location=invoiceObj.getValue('location');
                        log.audit('location',location);

                        var numLines = invoiceObj.getLineCount({
                         sublistId: 'item'
                        });
                        log.audit('Line Count',numLines);
                        
                        var scriptObj=runtime.getCurrentScript();
                        var scriptParamAPI=scriptObj.getParameter('custscript_last_consolidated_invoice_no');

                        invoiceRecord.setValue('customform', 114);
                        invoiceRecord.setValue('entity', customer);
                        invoiceRecord.setValue('trandate', parseAndFormatDateString(tranDate));
                        invoiceRecord.setValue('postingperiod', postingPeriod);
                        if (commercialInvoiceNumber) {
                            invoiceRecord.setValue('otherrefnum', commercialInvoiceNumber);
                        }
                        if (opportunity) {
                            invoiceRecord.setValue('opportunity', opportunity);
                        }
                        if (department) {
                            invoiceRecord.setValue('department', department);
                        }
                        if (location) {
                            invoiceRecord.setValue('location', location);
                        }
                        invoiceRecord.setValue('custbodycustbody_customertype', customerType);
                        invoiceRecord.setValue('custbody_modeoftransport', modeOfTransport);
                        if (serviceType) {
                            invoiceRecord.setValue('custbody_servicetype', serviceType);
                        }
                        invoiceRecord.setValue('custbody_businesstype', businessType);
                        //invoiceRecord.setValue('subsidiary', subsidiary);
                        invoiceRecord.setValue('custbody_importexportlicense', importExportLicense);
                        invoiceRecord.setValue('class', classificationClass);
                        invoiceRecord.getValue('class');
                        
                        getAndSetSublist(numLines,invoiceRecord,invoiceObj);

                    }else{
                        var invoiceObj = record.load({
                            type: 'invoice',
                            id: invoicesInternalIds[i],
                            isDynamic: true
                        });
                        log.audit('Selected Invoice[Secondary]',invoiceObj);

                        var numLines = invoiceObj.getLineCount({
                         sublistId: 'item'
                        });

                        getAndSetSublist(numLines,invoiceRecord,invoiceObj);
                    }
                }

                var consolidatedInvoiceId= invoiceRecord.save()
                if (consolidatedInvoiceId) {
                    log.audit('Consolidate Invoice ID', consolidatedInvoiceId);
                    for(var i=0;i<invoicesInternalIds.length;i++){
                        record.submitFields({
                            type: 'invoice',
                            id: invoicesInternalIds[i],
                            values: {
                                'custbody_consolidatedinvoice': consolidatedInvoiceId
                            }
                        });
                    }

                    var consolidatedInvoiceIds=new Array();
                    consolidatedInvoiceIds.push({
                        column1: consolidatedInvoiceId,
                        id: consolidatedInvoiceId,
                        column2: 'Consolidated Invoice Created Successfully'
                    });

                    //var invoiceCreatedUrl='https://5228963.app.netsuite.com/app/accounting/transactions/custinvc.nl?id='+consolidatedInvoiceId;

                    /*var form = serverWidget.createForm({
                        title: 'Consolidate Invoices'
                    });

                    var fldHtmlHeader = form.addField({
                        id: 'custpage_header',
                        label: 'Test',
                        type: serverWidget.FieldType.INLINEHTML
                    });
                    var htmlvar = '<html>'*/
                    //htmlvar += '<script type="text/javascript">';
                    //htmlvar += 'window.location = "' + invoiceCreatedUrl + '";'
                    //htmlvar += '</script>'
                    //htmlvar += '<h1 style="text-align: center;font-size: 25px; padding-bottom: 50px;">Invoice Created Successfully</h1>';
                    //htmlvar += '<div style="text-align: center;"><a href="'+invoiceCreatedUrl+'"padding-left: 90px;" target="_blank">View Invoice</a></div>';
                    //htmlvar += '<table><tr><th>Consolidated Invoice No.</th><th>Status</th></tr><tr><td>Link</td><td>Consolidated Invoice Created Successfully</td></tr></table>';
                    //htmlvar += '</html>';
                    //fldHtmlHeader.defaultValue = htmlvar;
                    //field.defaultValue = 'Invoice Created Successfully';

                    var list = serverWidget.createList({
                        title: 'Consolidated Invoice'
                    });

                    list.style = serverWidget.ListStyle.GRID;

                    var listColumn = list.addColumn({
                        id: 'column1',
                        type: serverWidget.FieldType.URL,
                        label: 'Consolidated Invoice',
                        align: serverWidget.LayoutJustification.LEFT
                    });

                    list.addColumn({
                        id: 'column2',
                        type: serverWidget.FieldType.URL,
                        label: 'Status',
                        align: serverWidget.LayoutJustification.LEFT
                    });

                    var URL = url.resolveRecord({
                        recordType: 'invoice'
                    });

                    listColumn.setURL({
                        url: URL
                    });

                    listColumn.addParamToURL({
                        param: 'id',
                        value: 'id',
                        dynamic: true
                    })

                    list.addRow(consolidatedInvoiceIds[0]);
                    
                    list.addPageLink({
                        title: 'Go To Consolidate Invoice',
                        type: serverWidget.FormPageLinkType.CROSSLINK,
                        url: 'https://5228963.app.netsuite.com/app/site/hosting/scriptlet.nl?script=268&deploy=1'
                    });

                    context.response.writePage(list);
                    //context.response.writePage(form);
                }else{
                    log.audit('failed to create consolidate invoice');
                }
            }
        }

        function getInvoices(pageSize,businessTypeParam,modeOfTransportParam,customerNameParam,customerTypeParam){
            var invoiceFilter=new Array();
            var invoiceColumns=new Array();
            log.emergency('Inside Function', 'getInvoices');

            invoiceFilter.push(search.createFilter({
                name: 'custbody_businesstype',
                operator: search.Operator.IS,
                values: businessTypeParam
            }));

            invoiceFilter.push(search.createFilter({
                name: 'customform',
                operator: search.Operator.NONEOF,
                values: 114
            }));

            invoiceFilter.push(search.createFilter({
                name: 'custbody_consolidatedinvoice',
                operator: search.Operator.ANYOF,
                values:'@NONE@'
            }));

            invoiceFilter.push(search.createFilter({
                name: 'approvalstatus',
                operator: search.Operator.IS,
                values:2
            }));  

            invoiceFilter.push(search.createFilter({
                name: 'custbody_modeoftransport',
                operator: search.Operator.IS,
                values: modeOfTransportParam
            }));  

            invoiceFilter.push(search.createFilter({
                name: 'custbodycustbody_customertype',
                operator: search.Operator.IS,
                values: customerTypeParam
            }));  

            invoiceFilter.push(search.createFilter({
                name: 'entity',
                operator: search.Operator.IS,
                values: customerNameParam
            }));

            invoiceFilter.push(search.createFilter({
                name: 'mainline',
                operator: search.Operator.IS,
                values: true
            }));

            invoiceColumns.push(search.createColumn('tranid')); 
            invoiceColumns.push(search.createColumn('custbody_servicetype'));
            invoiceColumns.push(search.createColumn('trandate'));

            var searchedInvoices = search.create({
                'type': 'invoice',
                'filters': invoiceFilter,
                'columns': invoiceColumns
            });

            var searchResults = searchedInvoices.runPaged({
                pageSize: pageSize
            });
            return searchResults;
        }

        function parseAndFormatDateString(myDate) {
            var initialFormattedDateString = myDate;
            var parsedDateStringAsRawDateObject = format.parse({
                value: initialFormattedDateString,
                type: format.Type.DATE
            });
            log.emergency('Date', parsedDateStringAsRawDateObject);
            return parsedDateStringAsRawDateObject;
        }

        function getAndSetSublist(numLines,invoiceRecord,invoiceObj){
            for(var j=0;j<numLines;j++){
                log.audit('The J', j);
                var sublistItemValue = invoiceObj.getSublistValue({
                 sublistId: 'item',
                 fieldId: 'item',
                 line: j
                });
                log.audit('sublist item value', typeof(sublistItemValue));
                var sublistQuantityValue = invoiceObj.getSublistValue({
                 sublistId: 'item',
                 fieldId: 'quantity',
                 line: j
                });
                var sublistDescriptionValue = invoiceObj.getSublistValue({
                 sublistId: 'item',
                 fieldId: 'description',
                 line: j
                });
                var sublistPriceValue = invoiceObj.getSublistValue({
                 sublistId: 'item',
                 fieldId: 'price',
                 line: j
                });
                var sublistRateValue = invoiceObj.getSublistValue({
                 sublistId: 'item',
                 fieldId: 'rate',
                 line: j
                });
                var sublistAmountValue = invoiceObj.getSublistValue({
                 sublistId: 'item',
                 fieldId: 'amount',
                 line: j
                });
                var sublistTaxcodeValue = invoiceObj.getSublistValue({
                 sublistId: 'item',
                 fieldId: 'taxcode',
                 line: j
                });
                invoiceRecord.selectNewLine({ 
                    sublistId: 'item'      
                });
                invoiceRecord.setCurrentSublistValue({
                 sublistId: 'item',
                 fieldId: 'item',
                 value: sublistItemValue
                });
                invoiceRecord.setCurrentSublistValue({
                 sublistId: 'item',
                 fieldId: 'quantity',
                 value: sublistQuantityValue
                });
                invoiceRecord.setCurrentSublistValue({
                 sublistId: 'item',
                 fieldId: 'amount',
                 value: sublistAmountValue
                });
                invoiceRecord.commitLine({ 
                    sublistId: 'item'
                });
            }
        }

        return {
            onRequest: onRequest
        };

});