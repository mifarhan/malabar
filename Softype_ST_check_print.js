/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Sep 2017     Saroja
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function CheckPrint(request, response){
	var action = request.getParameter('action');
	nlapiLogExecution('Debug','action',action);
	if(action == 'Checkprint')
	{
		var RecId = request.getParameter('recId');
		var RecType = request.getParameter('recType');
		var loadRec = nlapiLoadRecord(RecType,RecId);
		var vendor = loadRec.getFieldText('entity');
		var referenceNo = loadRec.getFieldValue('tranid');
		var date = loadRec.getFieldValue('trandate');
		var address = loadRec.getFieldValue('custbody_subsidiaryaddress');
		var subsidiary = loadRec.getFieldText('subsidiary');
		var name = loadRec.getFieldText('entity');
		var account = loadRec.getFieldText('account');
		var currency = loadRec.getFieldText('currency');
		var exchangerate = loadRec.getFieldValue('exchangerate');		
		var taxtotal = loadRec.getFieldValue('taxtotal');
		var businesstype = loadRec.getFieldText('custbody_businesstype');
		var modeoftransport = loadRec.getFieldValue('custbody_modeoftransport');
		var file = loadRec.getFieldValue('custbody_file');
		var usertotal = loadRec.getFieldValue('usertotal');
		var preparedby = loadRec.getFieldText('custbody_createdby');



				var image='https://5228963.app.netsuite.com/core/media/media.nl?id=1395&c=5228963&h=f94337f4f6508179777e';
				var html = '';
				html += '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">';
				html += '<pdf>';
				html += '<head>';
				html += '<macrolist>';
				
				html += '<macro id="nlheader">';
				
				html += '<table class="header" style="width: 100%;" margin-bottom="10px"><tr>';
				html += '<td rowspan="3">';
				html += '<div><img src="'+nlapiEscapeXML(image)+'" style="font-size: 12px; height: 60px; width:130px;margin: 5px" /></div></td>';
				html += '<td align="right" style="font-size:20px;width: 366px;">Check Voucher</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="right" style = "padding-top:10px;font-size: 12px;width: 366px;">Date : '+date+ '</td>';
				html += '</tr>';
				 html += '<tr>';
				html += '<td align="left" style = "padding-top:10px;font-size: 12px;width: 366px;"></td>';
				html += '</tr>'; 
				 html += '<tr>';
				html += '<td align="left" style = "padding-top:10px;font-size: 12px;width: 200px;">'+subsidiary+ '</td>';
				html += '</tr>'; 
				html += '<tr>';
				html += '<td align="left" style = "padding-top:10px;font-size: 12px;width: 200px;">'+(address == null? "" :address)+ '</td>';
				html += '</tr>';
				/* html += '<tr>';
				html += '<td align="right" style = "padding-top:10px">Paid To : ' +(address == null? "" :address)+ ' </td>';
				//html += '<td align="right" width="500" style = "padding-right:0px;">'+address+'</td>';
				html += '</tr>'; */
				html += '</table>';
				
				html += '<table style="width:300px">';
				html += '<tr><td colspan="2" align="left" style="font-size: 12px" ><b><u>Primary Information</u></b></td></tr>';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px" >Cheque#</td>';
				html += '<td align="left" style="padding-left:20px;font-size: 10px">'+referenceNo+ '</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px;">Account</td>';
				html += '<td align="left" style="padding-left:20px;font-size: 10px">'+account+ '</td>';
				html += '</tr>';
				html += '<tr>'; 
				html += '<td align="left" style="width:120px;font-size: 10px">Payee Name</td>';
				html += '<td style="padding-left:20px;text-align:left!important;font-size: 10px">'+name+'</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px">Amount</td>';
				html += '<td align="left" style="padding-left:20px;font-size: 10px">'+usertotal+'</td>';
				html += '</tr>';
				html += '</table>';	
				
				 html += '<table align="right" style="width:200px;padding-top:-100px">';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px">Currency</td>';
				html += '<td align="right" style="padding-left:-25px;font-size: 10px">'+currency+'</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px">Exchange Rate</td>';
				html += '<td align="right" style="padding-left:-25px;font-size: 10px">'+exchangerate+'</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px">Tax Total</td>';
				html += '<td align="right" style="padding-left:-25px;font-size: 10px">'+taxtotal+'</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px">Check Date</td>';
				html += '<td align="right" style="padding-left:-25px;font-size: 10px">'+date+'</td>';
				html += '</tr>'; 
				html += '</table>';			
				html += '</macro>'; 
				
				html += '<macro id="nlfooter">';
				html += '<table class="footer" style="width: 100%;"><tr>';
				html += '<td align="left" style="padding-left: 15px;font-size: 12px;"><b>Prepared by:</b> &nbsp;&nbsp;&nbsp;&nbsp;'+preparedby+'</td>';
				html += '<td align="right" style="font-size: 12px;"><b>Received by:</b>&nbsp;&nbsp;&nbsp;&nbsp; ___________________________ </td>';
				html += '</tr>';
				/* html += ' <tr>';
				html += ' <td style="padding-bottom: 30px;"></td>';
				html += '</tr>';
				html += '<tr>';
				html += ' <td align="left">'+preparedby+'</td>';
				html += '  <td align="right">___________________________</td>';
				html += ' </tr>'; */
				html += ' </table>';
				
				html += '</macro>';
				
				html += '</macrolist>';
				html += '<style type="text/css">* {';	
				html += 'table {';
				html += 'font-size: 8pt;';
				html += 'table-layout: fixed;';
				html += '}';
				html += 'th {';
				html += ' font-weight: bold;';
				html += ' font-size: 8pt;';
				html += 'vertical-align: middle;';
				html += 'padding: 5px 6px 3px;';
				html += ' background-color: #e3e3e3;';
				html += ' color: #333333;';
				html += '}';
				html += 'td {';
				html += ' padding: 4px 6px;';
				html += '}';
				html += 'td p { align:left }';
				html += 'b {';
				html += ' font-weight: bold;';
				html += ' color: #333333;';
				html += ' }';
				html += 'table.header td {';
				html += ' padding: 0px;';
				html += ' font-size: 10pt;';
				html += '}';
				html += 'table.footer td {';
				html += ' padding: 0px;';
				html += ' font-size: 8pt;';
				html += '}';
				html += 'table.itemtable th {';
				html += ' padding-bottom: 0px;';
				html += ' padding-top: 0px;';
				html += '}';
				html += 'table.body td {';
				html += 'padding-top: 2px;';
				html += '}';
				html += 'table.total {';
				html += 'page-break-inside: avoid;';
				html += '}';
				html += 'tr.totalrow {';
				html += '  background-color: #e3e3e3;';
				html += 'line-height: 200%;';
				html += '}';
				html += 'td.totalboxtop {';
				html += ' font-size: 12pt;';
				html += ' background-color: #e3e3e3;';
				html += '}';
				html += 'td.addressheader {';
				html += ' font-size: 8pt;';
				html += ' padding-top: 6px;';
				html += 'padding-bottom: 2px;';
				html += '}';
				html += 'td.address {';
				html += 'padding-top: 0px;';
				html += '}';
				html += 'td.totalboxmid {';
				html += ' font-size: 28pt;';
				html += 'padding-top: 20px;';
				html += ' background-color: #e3e3e3;';
				html += '}';
				html += 'td.totalboxbot {';
				html += ' background-color: #e3e3e3;';
				html += 'font-weight: bold;';
				html += '}';
				html += 'span.title {';
				html += 'font-size: 28pt;';
				html += '}';
				html += 'span.number {';
				html += 'font-size: 16pt;';
				html += '}';
				html += 'span.itemname {';
				html += 'font-weight: bold;';
				html += 'line-height: 150%;';
				html += '}';
				html += 'hr {';
				html += 'width: 100%;';
				html += 'color: #d3d3d3;';
				html += 'background-color: #d3d3d3;';
				html += 'height: 1px;';
				html += '}';
				html += '</style>';
				html += '</head>';

				html += '<body header ="nlheader" header-height="28%" footer="nlfooter" footer-height="53%" padding="0.5in 0.5in 0.5in 0.5in" size="Letter">';
				/* html += '<table border ="0" style="width: 200px;">';
				html += '<tr><td colspan="2" align="left" style="font-size: 10px"><b><u>Classification</u></b></td></tr>';
				html += '<tr>';
				html += '<td align="left"></td>';
				html += '<td align="right" style="padding-left:-25px;"></td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left"></td>';
				html += '<td  align="right" style="padding-left:-25px"></td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left" style="width:120px;font-size: 10px">Business Type</td>';
				html += '<td align="right" style="padding-left:-25px;font-size: 10px">'+ (businesstype == null? "" :businesstype)+ '</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px">Mode of transport</td>';
				html += '<td  align="right" style="padding-left:-25px;font-size: 10px">'+ (modeoftransport == null? "" :modeoftransport)+ '</td>';
				html += '</tr>';
				html += '<tr>';
				html += '<td align="left" style="font-size: 10px">File</td>';
				html += '<td align="right" style="padding-left:-25px;font-size: 10px">'+ (file == null? "" :file) +'</td>';
				html += '</tr>';				
				html += '</table>';	 */	
				html += '<br/>';	

				html += '<table border="1" class="itemtable" style="width: 100%;">';
				html += '<thead>';
				html += '<tr>';
				html += '<th align="center" style="width:120px;border-right: 1px; border-bottom: 1px;background-color: #e3e3e3;font-size: 10px"><b>Account</b></th>';
				html += '<th align="center" style="width:80px;border-right: 1px; border-bottom: 1px;background-color: #e3e3e3;font-size: 10px"><b>Amount</b></th>';
				html += '<th align="center" style="width:80px;border-right: 1px; border-bottom: 1px;background-color: #e3e3e3;font-size: 10px"><b>Tax Amt.</b></th>';
				html += '<th align="center" style="width:80px;border-right: 1px; border-bottom: 1px;background-color: #e3e3e3;font-size: 10px"><b>Gross Amt.</b></th>';
				html += '<th align="center" style="width:100px;border-right: 1px; border-bottom: 1px;background-color: #e3e3e3;font-size: 10px"><b>Memo</b></th>';
				html += '<th align="center" style="width:80px;border-right: 1px; border-bottom: 1px;background-color: #e3e3e3;font-size: 10px"><b>File</b></th>';
				html += '<th align="center" style="width:80px;border-right: 0px; border-bottom: 1px;background-color: #e3e3e3;font-size: 10px"><b>Employee</b></th>';
				
				html += '</tr>';
				html += '</thead>';
				
				var lineCount = loadRec.getLineItemCount('expense');
				if(lineCount > 0)
				{
					for(var i = 1; i <= lineCount; i++)
					{
						var description = loadRec.getLineItemValue('expense','account_display',i);
						var amount = loadRec.getLineItemValue('expense','amount',i);
						var taxamount = loadRec.getLineItemValue('expense','tax1amt',i);
						var grossamt = loadRec.getLineItemValue('expense','grossamt',i);
						var memo = loadRec.getLineItemValue('expense','memo',i);
						var job = loadRec.getLineItemValue('expense','custcol_jobline',i);
						var employee = loadRec.getLineItemValue('expense','custcol_employeecust',i);
						
						html += '<tr>';
						html += '<td align="left" style="width:120px;border-right: 1px;text-align:left!important;font-size: 10px">'+(description == null? "" :description)+'</td>';
						html += '<td align="right" style="width:80px;border-right: 1px;font-size: 10px">' +amount+ '</td>';
						html += '<td align="right" style="width:80px;border-right: 1px;font-size: 10px">' +taxamount+ '</td>';
						html += '<td align="right" style="width:80px;border-right: 1px;font-size: 10px">' +grossamt+ '</td>';
						html += '<td align="left" style="width:100px;border-right: 1px;text-align:left!important;font-size: 10px">'+(memo == null? "" :memo)+'</td>';
						html += '<td align="left" style="width:80px;border-right: 1px;text-align:left!important;font-size: 10px">' +(job == null? "" :job)+ '</td>';
						html += '<td align="left" style="width:80px;border-right: 0px;text-align:left!important;font-size: 10px">' + (employee == null? "" :employee)+ '</td>';
						html += '</tr>';
				}
				}
				html += '</table>';
				
				/* html += '<table class="total" style="width: 100%; margin-top: 10px;padding-right:70px"><tr>';
				html += '<td colspan="4">&nbsp;</td>';
				html += '<td align="right">Amount:</td>';
				html += '<td align="right">'+amount+'</td>';
				html += '</tr>';
				html += '</table>'; */
				html += '</body>';
				html += '</pdf>'; 

				nlapiLogExecution('Debug', 'HTML Layout', html);

				var file = nlapiXMLToPDF(html);
				response.setContentType('PDF','Print.pdf ','inline');
				response.write(file.getValue());

		}

}





