/**
/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 14/07/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : This is Script is just to Enable/Disable fields According to the value 

 **	of the Interim Total Field of that particular Item Master.

 **	To Acheive this when user select an Item, on its post sourcing a lookupField is triggered which checks wheather the Interim Total Field

 ** of that Item Master Have any value or not.

 ** And if it has any value then all the custom fields of 'Item' sublist are enabled & all the standard fields are disabled.

 ** And if Interim Total Field does not have any value then all the custom fields are disabled and standard fields remain as it is.

 **************************************************************************************************************************************/
var flagForScriptExecution=false;
function postSourcing(type,name){
	//var flagForScriptExecution=checkForScriptTrigger();
	//alert(flagForScriptExecution)
	if (!flagForScriptExecution) {
        return;
    }
	if (type=='item' && name=='item') {
		var itemSelected=nlapiGetCurrentLineItemValue('item','item')
		if (!itemSelected) {
			return;
		}
		var interimFormula=nlapiLookupField('serviceitem',itemSelected,'custitem_iteminterimtotal');
		if (interimFormula!=null && interimFormula!=undefined && interimFormula!='') {
			nlapiDisableLineItemField('item','custcol_splitcif',false);
			nlapiDisableLineItemField('item','custcol_dutyrate',false);
			nlapiDisableLineItemField('item','custcol_customvat',false);
			nlapiDisableLineItemField('item','custcol_cdfrate',false);
			nlapiDisableLineItemField('item','amount',true);
			nlapiDisableLineItemField('item','quantity',true);
			nlapiDisableLineItemField('item','rate',true);
			nlapiDisableLineItemField('item','taxcode',true);
			nlapiDisableLineItemField('item','tax1amt',true);
			nlapiDisableLineItemField('item','grossamt',true);
		}else{
			nlapiDisableLineItemField('item','custcol_splitcif',true);
			nlapiDisableLineItemField('item','custcol_dutyrate',true);
			nlapiDisableLineItemField('item','custcol_cdfrate',true);
			nlapiDisableLineItemField('item','custcol_interimtotal',true);
			nlapiDisableLineItemField('item','custcol_vatamount',true);
			nlapiDisableLineItemField('item','custcol_totalcdfandvat',true);
			nlapiDisableLineItemField('item','custcol_customvat',true);
		}
	}
}

function fieldChange(type,name){
	if (!flagForScriptExecution) {
        return;
    }
	if (type=='item' && name=='item') {
		var itemSelected=nlapiGetCurrentLineItemValue('item','item');
		if (!itemSelected) {
			var line=nlapiGetCurrentLineItemIndex('item');
			if (line) {
				nlapiRemoveLineItem('item',line);
			}
		}
	}
}

function lineInit(type){
	if (!flagForScriptExecution) {
        return;
    }
	var con=nlapiGetContext();
	if (type=='item') {
		var line=nlapiGetCurrentLineItemIndex('item')
		var itemSelected=nlapiGetLineItemValue('item','item',line);
		if (!itemSelected) {
			nlapiDisableLineItemField('item','custcol_splitcif',true);
			nlapiDisableLineItemField('item','custcol_dutyrate',true);
			nlapiDisableLineItemField('item','custcol_cdfrate',true);
			nlapiDisableLineItemField('item','custcol_interimtotal',true);
			nlapiDisableLineItemField('item','custcol_vatamount',true);
			nlapiDisableLineItemField('item','custcol_totalcdfandvat',true);
			nlapiDisableLineItemField('item','custcol_customvat',true);
			return;
		}
		var interimFormula=nlapiLookupField('serviceitem',itemSelected,'custitem_iteminterimtotal');
		if (interimFormula!=null && interimFormula!=undefined && interimFormula!='') {
			nlapiDisableLineItemField('item','custcol_splitcif',false);
			nlapiDisableLineItemField('item','custcol_dutyrate',false);
			nlapiDisableLineItemField('item','custcol_customvat',false);
			nlapiDisableLineItemField('item','custcol_cdfrate',false);
			nlapiDisableLineItemField('item','amount',true);
			nlapiDisableLineItemField('item','quantity',true);
			nlapiDisableLineItemField('item','rate',true);
			nlapiDisableLineItemField('item','taxcode',true);
			nlapiDisableLineItemField('item','tax1amt',true);
			nlapiDisableLineItemField('item','grossamt',true);
		}else{
			nlapiDisableLineItemField('item','custcol_splitcif',true);
			nlapiDisableLineItemField('item','custcol_dutyrate',true);
			nlapiDisableLineItemField('item','custcol_cdfrate',true);
			nlapiDisableLineItemField('item','custcol_interimtotal',true);
			nlapiDisableLineItemField('item','custcol_vatamount',true);
			nlapiDisableLineItemField('item','custcol_totalcdfandvat',true);
			nlapiDisableLineItemField('item','custcol_customvat',true);
		}
	}
}

function pageInit(){
	//ar flagForScriptExecution=false;
	console.log('start');
	var formSelected=nlapiGetFieldValue('customform')
	var recordType=nlapiGetRecordType();
	var recordIdForForms;
    if (recordType=='salesorder') {
        recordIdForForms=2;
    }else if(recordType=='invoice'){
        recordIdForForms=4;
    }else if(recordType=='estimate'){
        recordIdForForms=1;
    }
    if (recordIdForForms) {
        var formNames = nlapiLookupField('customrecord_formlist',recordIdForForms,'custrecord_formname')
        var forms= formNames.split(',');
        for(var i=0;i<forms.length;i++){
            //alert('forms[i].value==>'+forms[i].value+' formSelected==>'+formSelected);
            if (forms[i]==formSelected) {
                //alert(forms[i].value);
                flagForScriptExecution=true;
                break;
            }
        }
        //alert(JSON.stringify(forms))
    }
    //return flagForScriptExecution;
}