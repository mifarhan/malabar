/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
/************************************************************************************************************************************
 ** Copyright (c) 1998-2019 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  
 **                       
 ** @Author      : Farhan S
 ** @Dated       : 08/07/2019   DD/MM/YYYY
 ** @Version     : 1.0
 ** @Description : ClientScript Deployed on Transactions['Quotation','SalesOrder','Invoice'] To Perform Operations on Line Items.  
 **************************************************************************************************************************************/
define(['N/currentRecord','N/https','N/url','N/format','N/ui/message','N/search','N/runtime','N/record'],
function(currentRecord,https,url,format,message,search,runtime,record) {
    //"mode" is used globally to get the context in which script is being triggered - create,copy,edit,...
    var mode;
    var formulaForInterim,formulaForInterimValidateLine;
    var amountBefore;
    var flagForScriptExecution=false;
    
    function pageInit(scriptContext) {
        //alert('pageInit')
        mode = scriptContext.mode;
        var currentRecordObj = currentRecord.get();
        var formSelected=currentRecordObj.getValue('customform');
        //log.debug('mode', mode);
        /*var record = scriptContext.currentRecord;
        var itemCount = record.getLineCount('item');*/
        //alert(currentRecordObj.type)
        var recordType=currentRecordObj.type;
        var recordIdForForms;
        if (recordType=='salesorder') {
            recordIdForForms=2;
        }else if(recordType=='invoice'){
            recordIdForForms=4;
        }else if(recordType=='estimate'){
            recordIdForForms=1;
        }

        if (recordIdForForms) {
            var formNames = search.lookupFields({
                type: 'customrecord_formlist',
                id: recordIdForForms,
                columns: ['custrecord_formname']
            });
            var forms= formNames.custrecord_formname;
          log.emergency('forms',forms)
            for(var i=0;i<forms.length;i++){
                
                if (forms[i].value==formSelected) {
                    //alert(forms[i].value);
                    flagForScriptExecution=true;
                    break;
                }
            }
            //alert(JSON.stringify(forms))
        }
        /*var interimTotalFormula = search.lookupFields({
            type: 'customrecord_formlist',
            id: selectedItemIid,
            columns: ['custitem_iteminterimtotal']
        });
        var formula= interimTotalFormula.custitem_iteminterimtotal; */
    }
    function lineInit(scriptContext) {
        //alert('Line Init '+mode)
        if (!flagForScriptExecution) {
            return;
        }
        var currentRecordObj = currentRecord.get();
        if (mode=='edit' || mode=='copy') {
            getInterimFormula(scriptContext,currentRecordObj)
            /*var lineNo=scriptContext.line;
            var selectedItemIid=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'item'});
            if (!selectedItemIid) {
                return;
            }
            //alert('Line Item Value'+selectedItemIid);
            var interimTotalFormula = search.lookupFields({
                type: 'serviceitem',
                id: selectedItemIid,
                columns: ['custitem_iteminterimtotal']
            });
            var formula= interimTotalFormula.custitem_iteminterimtotal; 
            //alert('interim Total Formula'+formula);
            if (formula=='custcol_splitcif+custcol_cdfrate') {
                //alert('Formula Matched A');
                formulaForInterimValidateLine=formulaForInterim='A';
            }else if (formula == 'custcol_cdfrate') {
                //alert('Formula Matched B');
                formulaForInterimValidateLine=formulaForInterim='B';
            }else{
                formulaForInterimValidateLine=formulaForInterim='';
            }*/
        }
    }
    function fieldChanged(scriptContext){
        //alert(JSON.stringify(scriptContext));
        if (!flagForScriptExecution) {
            return;
        }
        var businessTypeValue,modeOfTransport,customerType,customerName;
        var currentRecordObj = currentRecord.get();
        //log.debug('currentRecord FieldChanged',currentRecord)
        if(scriptContext.fieldId == 'item'){
            //alert('fieldChanged');
            //alert('Item value changed'+JSON.stringify(scriptContext));
            getInterimFormula(scriptContext,currentRecordObj)
            /*var lineNo=scriptContext.line;
            var selectedItemIid=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'item'});
            if (!selectedItemIid) {
                return;
            }
            //alert('Line Item Value'+selectedItemIid);
            var interimTotalFormula = search.lookupFields({
                type: 'serviceitem',
                id: selectedItemIid,
                columns: ['custitem_iteminterimtotal']
            });
            var formula= interimTotalFormula.custitem_iteminterimtotal; 
            //alert('interim Total Formula'+formula);
            if (formula=='custcol_splitcif+custcol_cdfrate') {
                //alert('Formula Matched A');
                formulaForInterimValidateLine=formulaForInterim='A';
            }else if (formula == 'custcol_cdfrate') {
                //alert('Formula Matched B');
                formulaForInterimValidateLine=formulaForInterim='B';
            }else{
                formulaForInterimValidateLine=formulaForInterim='';
            }*/
        }
        // A * B = C
        if (scriptContext.fieldId == 'custcol_splitcif') {
            var dutyRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_dutyrate'});
            //alert(dutyRate)
            if (dutyRate) {
                var dutyStr=dutyRate.toString();
                if(dutyStr.endsWith('%')){
                    dutyRate=parseFloat(dutyRate) 
                    dutyRate=dutyRate/100;
                }
                var cif=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif'});
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate',value:cif*dutyRate});
            }
        }
        if (scriptContext.fieldId == 'custcol_dutyrate') {
            var cif=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif'});
            if (cif) {
                var dutyRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_dutyrate'});
                var dutyStr=dutyRate.toString();
                if(dutyStr.endsWith('%')){
                    dutyRate=parseFloat(dutyRate)
                    dutyRate=dutyRate/100;
                }
                //dutyRate=dutyRate/100;
                //alert(dutyRate)
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate',value:cif*dutyRate});
            }
        }

        // A + C or C = D
        if (scriptContext.fieldId == 'custcol_cdfrate') {
            if (formulaForInterim=='A') {
                var cif=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif'});
                var cdf=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate'});
                //if (cif && cdf) {
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal',value:cif+cdf});
                //}
            }else if (formulaForInterim=='B') {
                //var cif=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif'});
                var cdf=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate'});
                if (cdf) {
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal',value:cdf});
                }
            }
            var enteredCdfRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate'});
            if (enteredCdfRate) {
                if (formulaForInterim=='A' || formulaForInterim=='B') {
                    var exchangeRate=currentRecordObj.getValue('custbody_customexchangerate');
                    if (exchangeRate) {
                        //alert('Dividing by Exchange Rate'+exchangeRate)
                        var dividedCdf=enteredCdfRate/exchangeRate
                        currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value:Number(dividedCdf.toFixed(2))});
                        currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'amount',value:enteredCdfRate/exchangeRate});
                    }else{
                        //alert('No Exchange Rate'+exchangeRate)
                        currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value:enteredCdfRate});
                        currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'amount',value:enteredCdfRate});
                    }
                }else{
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value:enteredCdfRate});
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'amount',value:enteredCdfRate});
                }
            }
        }
        //select VAT %
        if (scriptContext.fieldId == 'custcol_customvat') {
            var selecctedVAT=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_customvat'});
            //if (selecctedVAT) {
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'taxcode',value:selecctedVAT});
            //}
        }
        //on Change of Tax Amount get the value of Tax Amount
        if (scriptContext.fieldId == 'taxrate1') {
            var taxAmt=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'tax1amt'});
            var cif=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif'});
            //alert('tax1amt Changed '+ taxAmt)
            if (!cif && !formulaForInterim) {
                //alert('tax1amt Changed RETURN')
                return;
            }
            if (taxAmt || taxAmt==0 ) {
                var exchangeRate=currentRecordObj.getValue('custbody_customexchangerate');
                if (exchangeRate) {
                    taxAmt=taxAmt*exchangeRate;
                }
                //alert('tax1amt Changed TAX AMOUNT'+taxAmt)
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount',value:taxAmt});
            }
        }
        if (scriptContext.fieldId == 'custcol_vatamount') {
            var vatAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount'});
            if (vatAmount || vatAmount==0) {
                var cdfRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate'});
                if (cdfRate) {
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat',value:vatAmount+cdfRate});
                }
            }
        }
        if (scriptContext.fieldId == 'custcol_cdfrate') {
            var vatAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount'});
            if (vatAmount || vatAmount==0) {
                var cdfRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate'});
                if (cdfRate) {
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat',value:vatAmount+cdfRate});
                }
            }
        }
        //when user come again and change the split cif the vat amount should be auto updated
        if (scriptContext.fieldId == 'custcol_splitcif' || scriptContext.fieldId == 'custcol_cdfrate') {
            var splitCif=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif'});
            var vatPercent=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_customvat'});
            if (splitCif && vatPercent) {
                var taxAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'tax1amt'});
                //alert('splitcif tax amount'+taxAmount)
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount',value:taxAmount});
            }
        }

        if (scriptContext.fieldId == 'custcol_splitcif' || scriptContext.fieldId == 'custcol_dutyrate' || scriptContext.fieldId == 'taxrate1' || scriptContext.fieldId == 'custcol_cdfrate') {
            if (scriptContext.fieldId == 'custcol_splitcif' || scriptContext.fieldId == 'custcol_dutyrate' || scriptContext.fieldId == 'custcol_cdfrate') {
                //alert('CifDutyTax')
                var dutyRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_dutyrate'});
                var customVat=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_customvat'});
                var splitCif=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif'});

                if (dutyRate!=0 && customVat!=0 ) {
                    var taxRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'taxrate1'});
                    //alert(splitCif);
                    taxRate=taxRate/100;
                    //alert(taxRate)
                    var interim=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal'});
                    var vatAmount=interim*taxRate;
                    //alert(vatAmount)
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount',value:vatAmount});
                }

                /*if (splitCif==0 || dutyRate==0) {
                    //alert('split')
                    //currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal',value:0});
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount',value:0});
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat',value:0});
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'quantity',value:1});
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value:0});
                }*/
            }else{
                var splitCif=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif'});
                var dutyRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_dutyrate'});
                
                if (splitCif && dutyRate) {
                    var taxRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'taxrate1'});
                    //alert(taxRate);
                    taxRate=taxRate/100;
                    //alert(taxRate)
                    var interim=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal'});
                    var vatAmount=interim*taxRate;
                    //alert(vatAmount)
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount',value:vatAmount});
                }
            }
        }

        if (scriptContext.fieldId == 'custcol_vatamount') {
            var vatAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount'});
            var totalCdfAndVat=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat'});
            //alert('post Sourcing '+ vatAmount)
            var exchangeRate=currentRecordObj.getValue('custbody_customexchangerate');
            if (exchangeRate) {
                var taxAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'tax1amt'});
                if (taxAmount!=vatAmount/exchangeRate) {
                    currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'tax1amt',value:vatAmount/exchangeRate});
                }
            }else{
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'tax1amt',value:vatAmount});
            }
        }

        if (scriptContext.fieldId == 'custbody_customexchangerate') {
            //var recordObj=record.get();
            var exchangeRate=currentRecordObj.getValue('custbody_customexchangerate');

            var record = scriptContext.currentRecord;
            var itemCount = record.getLineCount('item');
            //alert('itemCount '+itemCount)

            if (itemCount>0) { //&& exchangeRate
                for(var i=0;i<itemCount;i++){
                    record.selectLine({sublistId: 'item',line: i});
                    var interim=record.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal'});
                    if (interim) {
                        var cdfRate=record.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate'});
                        var rate=record.getCurrentSublistValue({sublistId:'item',fieldId:'rate'});
                        var amount=record.getCurrentSublistValue({sublistId:'item',fieldId:'amount'});
                        if (exchangeRate) {
                            if (rate!=cdfRate/exchangeRate && amount!=cdfRate/exchangeRate) {
                                var dividedCdf=cdfRate/exchangeRate
                                record.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value:Number(dividedCdf.toFixed(2))});
                                record.setCurrentSublistValue({sublistId:'item',fieldId:'amount',value:cdfRate/exchangeRate});
                                record.commitLine({sublistId: 'item'});
                            }
                        }else{
                            record.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value:cdfRate});
                            record.setCurrentSublistValue({sublistId:'item',fieldId:'amount',value:cdfRate});
                        }
                    }
                }
            }
        }

        if (scriptContext.fieldId =='rate' || scriptContext.fieldId =='amount') {
            //alert('RATE || AMOUNT')
            var interim=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal'});
            var exchangeRate=currentRecordObj.getValue('custbody_customexchangerate');
            var vatAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount'});
            if (interim && exchangeRate) {
                //alert('tax amt',vatAmount)
                //var totalCdfAndVat=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat'});
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'tax1amt',value:vatAmount/exchangeRate});
                //currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'grossamt',value:totalCdfAndVat});
            }else if (interim && exchangeRate=='') {
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'tax1amt',value:vatAmount});
            }
        }
    }

    function getInterimFormula(scriptContext,currentRecordObj){
        var lineNo=scriptContext.line;
        var selectedItemIid=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'item'});
        if (!selectedItemIid) {
            return;
        }
        //alert('Line Item Value'+selectedItemIid);
        var interimTotalFormula = search.lookupFields({
            type: 'serviceitem',
            id: selectedItemIid,
            columns: ['custitem_iteminterimtotal']
        });
        var formula= interimTotalFormula.custitem_iteminterimtotal; 
        //alert('interim Total Formula'+formula);
        if (formula=='custcol_splitcif+custcol_cdfrate') {
            //alert('Formula Matched A');
            formulaForInterimValidateLine=formulaForInterim='A';
        }else if (formula == 'custcol_cdfrate') {
            //alert('Formula Matched B');
            formulaForInterimValidateLine=formulaForInterim='B';
        }else{
            formulaForInterimValidateLine=formulaForInterim='';
        }
    }

    function validateLine(context) {
        var currentRecordObj = currentRecord.get();
        var exchangeRate=currentRecordObj.getValue('custbody_customexchangerate');
        var cdfRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate'});
        var vatAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount'});
        var interim=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal'});
        var totalCdfAndVat=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat'});
        //alert('INTERIM Formula '+exchangeRate)
        if (formulaForInterimValidateLine) {
            var interim=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal'});
            /*if (interim==0) {
                alert('Please Enter Value For Required Fields');
                return false;
            }*/
        }
        if (currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'quantity'})==0) {
            alert('Quantity Can Not Be 0');
            return false;
        }
        if (currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'rate'})==0) {
            alert('Rate Can Not Be 0');
            return false;
        }
        if (exchangeRate && formulaForInterimValidateLine) {
            if (rate!=cdfRate/exchangeRate && amount!=cdfRate/exchangeRate) {
                var vatAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount'});
                var cdfRate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate'});
                var rate=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'rate'});
                var amount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'amount'});
                var dividedCdf=cdfRate/exchangeRate;
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value:Number(dividedCdf.toFixed(2))});
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'amount',value:cdfRate/exchangeRate});
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'tax1amt',value:vatAmount/exchangeRate});
                //record.commitLine({sublistId: 'item'});
            }
        }
        if (formulaForInterimValidateLine) {
            currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_cdfrate',value:Number(cdfRate.toFixed(2))});
            currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount',value:Number(vatAmount.toFixed(2))});
            currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_interimtotal',value:Number(interim.toFixed(2))});
            currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat',value:Number(totalCdfAndVat.toFixed(2))});
        }
        formulaForInterimValidateLine='';
        return true;
    }

    function postSourcing(scriptContext){
        if (!flagForScriptExecution) {
            return;
        }
        //alert('post Sourcing '+JSON.stringify(scriptContext))
        var currentRecordObj = currentRecord.get();
        if (scriptContext.fieldId=='item') {
            //alert('postSourcing');
            currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif',value:''});
            currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_dutyrate',value:0});
            //currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_splitcif',value:0});
            currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'quantity',value:1});
            currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_customvat',value:''});
        }
        if (scriptContext.fieldId=='taxcode' && formulaForInterim) {
            //alert('TAX CODE '+formulaForInterim)
            var exchangeRate=currentRecordObj.getValue('custbody_customexchangerate');
            var vatAmount=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_vatamount'});
            var totalCdfAndVat=currentRecordObj.getCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat'});
            //alert('post Sourcing '+ vatAmount)
            if (exchangeRate) {
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'tax1amt',value:vatAmount/exchangeRate});
            }else{
                currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'tax1amt',value:vatAmount});
            }
            //currentRecordObj.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_totalcdfandvat',value:totalCdfAndVat});
        }
    }
    
    return {
        pageInit: pageInit,
        fieldChanged:fieldChanged,
        lineInit:lineInit,
        validateLine:validateLine,
        postSourcing:postSourcing
    };
});