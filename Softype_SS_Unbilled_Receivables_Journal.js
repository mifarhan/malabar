/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.                                 
 ** Morvi House, 30 Goa Street, Ballard Estate, Mumbai 400 001, India
 ** All Rights Reserved.                                                    
 **                                                                         
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information"). 
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.                  
 **                       
 **@Author      :  Farhan Shaikh
 **@Dated       :  27/08/2019
 **@Version     :  1.0
 **@Description : Schedule Script to create Journal Entries Against Journals
 ***************************************************************************************/
define(['N/runtime', 'N/record', 'N/search', 'N/task','N/email'],

    function(runtime, record, search, task,email) {
        function execute(context){
            log.emergency('ScheduledScript')
            var journalSearch=search.load('customsearch252').run().getRange({start:0,end:1000});
            log.emergency('journalSearch',journalSearch.length)
            if (journalSearch) {
            	for(var i=0;i<journalSearch.length;i++){
                    try{
                        log.emergency('journalSearch',journalSearch)
                        var jvRecordObj=record.create({
                            type:'journalentry',
                            isDynamic:true
                        });
    
                        var journalId=journalSearch[i].getValue('internalid')
                        var line=journalSearch[i].getValue('line')
                        log.emergency('journalId',journalId)
                        log.emergency('line',line)
                        var account=journalSearch[i].getValue('account')
                        var debitAmount=journalSearch[i].getValue('amount')
                        var businessType=journalSearch[i].getValue('custbody_businesstype')
                        var customer=journalSearch[i].getValue('custbody_customerlistoninvoice')
                        var modeOfTransport=journalSearch[i].getValue('custbody_modeoftransport')
                        var subsidiary=journalSearch[i].getValue('subsidiary')
                        var file=journalSearch[i].getText('custcol_file')
                        log.emergency('customer',customer)
    
                        var relatedAccount = search.lookupFields({
                            type: 'account',
                            id: account,
                            columns: ['custrecord_relatedaccount']
                        });
                        log.emergency('relatedAccount',relatedAccount)
    
                        if (relatedAccount) {
                            jvRecordObj.setValue('customform',110);
                            jvRecordObj.setValue('subsidiary',subsidiary);
                            jvRecordObj.setValue('custbody_customerlistoninvoice',customer);                                
                            jvRecordObj.setValue('custbody_businesstype',businessType);
                            jvRecordObj.setValue('custbody_modeoftransport',modeOfTransport);
                            jvRecordObj.setValue('currency',1);
                            jvRecordObj.setText('custbody_file',file);
                            //jvRecordObj.setValue('memo',memo);
                            jvRecordObj.setValue('custbody_jvcreated',true);
                            jvRecordObj.setValue('approvalstatus',2);
                            jvRecordObj.setValue('custbody_approve_status',1);
    
                            jvRecordObj.selectNewLine({ 
                                sublistId: 'line'      
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'account',
                             value: account
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'credit',
                             value: debitAmount
                            });
                            jvRecordObj.commitLine({ 
                                sublistId: 'line'
                            });
                            jvRecordObj.selectNewLine({ 
                                sublistId: 'line'      
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'account',
                             value: relatedAccount.custrecord_relatedaccount[0].value
                            });
                            jvRecordObj.setCurrentSublistValue({
                             sublistId: 'line',
                             fieldId: 'debit',
                             value: debitAmount
                            });
                            jvRecordObj.commitLine({ 
                                sublistId: 'line'
                            });
    
                            var copyJournalId=jvRecordObj.save()
                            log.debug('copyJournalId',copyJournalId)
                        }
                        var journalObj=record.load({
                            type:'journalentry',
                            id: journalId,
                            //isDynamic:true
                        })
    
                        var numLines = journalObj.getLineCount({
                         sublistId: 'line'
                        });
    
                        log.debug('expense Line Count',numLines)
    
                        var journalRef=search.lookupFields({
                            type:'journalentry',
                            id:copyJournalId,
                            columns: ['tranid']
                        })
    
                        log.debug('journalRef',journalRef)
    
                        var sublistItemValue = journalObj.setSublistValue({
                         sublistId: 'line',
                         fieldId: 'custcol_automaticjv',
                         line: line,
                         value:journalRef.tranid
                        });
    
                        var journal=journalObj.save()
                        log.debug('journal',journal)
                        var remainingUsage = runtime.getCurrentScript().getRemainingUsage();
                        log.audit("Remaining Usage", remainingUsage);
    
                        // Check remaining usage and reschedule if necessary
                        if (remainingUsage < 100) {
                            var taskId = rescheduleCurrentScript();
                            log.audit("Rescheduling status: " + task.checkStatus(taskId));
                            return;
                        }
                    }catch(e){
                        if (e instanceof nlobjError) {
                         
                         log.debug("Error Code= "+e.getCode(),"Error Details= "+e.getDetails());
                         
                        }else{
                         
                         log.debug("Error Message= ",e.toString());
                         
                        }
                    }
            	}
            }else{
            	log.emergency('journalSearch','Empty')
            }
        }

        function rescheduleCurrentScript() {
            var scheduledScriptTask = task.create({
                taskType: task.TaskType.SCHEDULED_SCRIPT
            });
            scheduledScriptTask.scriptId = runtime.getCurrentScript().id;
            scheduledScriptTask.deploymentId = runtime.getCurrentScript().deploymentId;
            return scheduledScriptTask.submit();
        }

        return {
            execute: execute
        };

    });