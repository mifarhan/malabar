/**

 * @NApiVersion 2.x

 * @NScriptType UserEventScript

 * @NModuleScope SameAccount

 */

/************************************************************************************************************************************

 ** Copyright (c) 1998-2019 Softype, Inc.

 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

 ** All Rights Reserved.

 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

 ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

 **                       

 ** @Author      : Farhan S

 ** @Dated       : 16/08/2019   DD/MM/YYYY

 ** @Version     : 

 ** @Description : UserEvent Script on Invoice (File) to set the Transaction Status.  

 **************************************************************************************************************************************/
define(['N/record', 'N/runtime', 'N/search', 'N/format'],

    function(record, runtime, search, format) {
    
        function beforeSubmit(scriptContext) {

        }



        /**

         * Function definition to be triggered after record is submitted.

         *

         * @param {Object} scriptContext

         * @param {Record} scriptContext.newRecord - New record

         * @param {Record} scriptContext.oldRecord - Old record

         * @param {string} scriptContext.type - Trigger type

         * @Since 2015.2

         */

        function afterSubmit(scriptContext) {
            try{
                log.audit('executionContext.type', runtime.executionContext);
                log.debug('scriptContext.type', scriptContext.type);
    
                var currentRecordObj = scriptContext.newRecord;
                log.debug('CurrRecObj', currentRecordObj);

                var formSelected=currentRecordObj.getValue('customform')

                var formNames = search.lookupFields({
                    type: 'customrecord_jvcreationformlist',
                    id: 1,
                    columns: ['custrecord_frmname']
                });
                var forms= formNames.custrecord_frmname;
                log.emergency('forms',forms)
                for(var i=0;i<forms.length;i++){
                    
                    if (forms[i].value==formSelected) {
                        //alert(forms[i].value);
                        break;
                    }else if (i==forms.length-1) {
                        log.emergency('Returning','Form is Different')
                        return;
                    }
                }
    
                //var status = currentRecordObj.getValue('status')
                var file = currentRecordObj.getValue('id')
                //log.debug('status', status);
                log.debug('file', file);

                var statusLookup=search.lookupFields({
                    type:'invoice',
                    id:file,
                    columns:['status']
                })  
                var status=statusLookup.status[0].text
                //status=status.status[0].text;
                log.debug('status lookup',statusLookup.status[0].text)

                if (status != 'Approved' && status != 'Open') {
                    log.debug('Returning', 'NOT Approved');
                    return;
                }else{
                    var checkFilter=[];
                    //var checkColumns=[];
    
                    var searchObj=search.load({id:'customsearch234'});
    
                    /*checkFilter.push(search.createFilter({
                        name: 'custbody_file',
                        operator: 'is',
                        values: file
                    }));*/
    
                    searchObj.filters .push(search.createFilter({
                        name: 'custbody_file',
                        operator: 'anyof',
                        values: file
                    }));
    
                    var searchTrigger=searchObj.run().getRange({start:0,end:1000});
                    log.debug('First search Result',searchTrigger);
                    if (searchTrigger) {
                        //Found Check For This Perticular File
                        for(var i=0;i<searchTrigger.length;i++){
                            var docNo=searchTrigger[i].getValue({name:'internalid',summary:'group'});
                            log.debug('docNo',docNo)
                            var searchIIDObj=search.load({id:'customsearch237'});
                            searchIIDObj.filters.push(search.createFilter({
                                name: 'internalid',
                                operator: 'anyof',
                                values: docNo
                            }));
                            var searchIIDTrigger=searchIIDObj.run().getRange({start:0,end:1000});
                            var jvRecordObj=record.create({
                                type:'journalentry',
                                isDynamic:true
                            });
                            var doctype;
                            for(var j=0;j<searchIIDTrigger.length;j++){
                                if (j==0) {

                                }
                                var type=searchIIDTrigger[j].getValue('type')
                                doctype=type;
                                var debit=searchIIDTrigger[j].getValue('debitamount')
                                var account=searchIIDTrigger[j].getValue('account')
                                var file=searchIIDTrigger[j].getValue('custbody_file')
                                var currency=searchIIDTrigger[j].getValue('currency')
                                var exchangerate=searchIIDTrigger[j].getValue('exchangerate')
                                var subsidiary=searchIIDTrigger[j].getValue('subsidiary')
                                var businessType=searchIIDTrigger[j].getValue('custbody_businesstype')
                                var modeOfTransport=searchIIDTrigger[j].getValue('custbody_modeoftransport')
                                var memo=searchIIDTrigger[j].getValue('memo')
                                var customer=searchIIDTrigger[j].getValue('custbody_customerlistoninvoice')
                                var relatedAccount = search.lookupFields({
                                    type: 'account',
                                    id: account,
                                    columns: ['custrecord_relatedaccount']
                                });
                                log.debug('Accounts','Related Account : '+JSON.stringify(relatedAccount)+' Account : '+account)
                                //log.debug(relatedAccount.custrecord_relatedaccount[0].value)
                                //log.debug('Type : '+type+' docNo : '+docNo+' Account :'+account+' Credit :'+credit+' Debit : '+debit);
                                
                                if (type=='Check' || type=='Journal') {
                                    jvRecordObj.setValue('customform',110);
                                    jvRecordObj.setValue('exchangerate',exchangerate);
                                    jvRecordObj.setValue('subsidiary',subsidiary);
                                    jvRecordObj.setValue('custbody_customerlistoninvoice',customer);                                
                                    jvRecordObj.setValue('currency',currency);
                                    jvRecordObj.setValue('custbody_file',file);
                                    jvRecordObj.setValue('memo',memo);
                                    jvRecordObj.setValue('custbody_businesstype',businessType);
                                    jvRecordObj.setValue('custbody_modeoftransport',modeOfTransport);
                                    jvRecordObj.setValue('custbody_jvcreated',true);
                                    jvRecordObj.setValue('approvalstatus',2);
                                    jvRecordObj.setValue('custbody_approve_status',1);
                                    
    
                                    if (relatedAccount) {
                                        jvRecordObj.selectNewLine({ 
                                            sublistId: 'line'      
                                        });
                                        jvRecordObj.setCurrentSublistValue({
                                         sublistId: 'line',
                                         fieldId: 'account',
                                         value: account
                                        });
        
                                        jvRecordObj.setCurrentSublistValue({
                                         sublistId: 'line',
                                         fieldId: 'credit',
                                         value: debit
                                        });
    
                                        jvRecordObj.commitLine({ 
                                            sublistId: 'line'
                                        });
    
                                        jvRecordObj.selectNewLine({ 
                                            sublistId: 'line'      
                                        });
    
                                        jvRecordObj.setCurrentSublistValue({
                                         sublistId: 'line',
                                         fieldId: 'account',
                                         value: relatedAccount.custrecord_relatedaccount[0].value
                                        });
        
                                        jvRecordObj.setCurrentSublistValue({
                                         sublistId: 'line',
                                         fieldId: 'debit',
                                         value: debit
                                        });
    
                                        jvRecordObj.commitLine({ 
                                            sublistId: 'line'
                                        });
                                    }
                                }
                            }
                            var journalId=jvRecordObj.save()
                            if (journalId) {
                                if (doctype=='Journal') {
                                    record.submitFields({
                                        type: 'journalentry',
                                        id: docNo,
                                        values: {
                                            'custbody_jvcreated': true
                                        }
                                    });
                                }else if(doctype=='Check'){
                                    record.submitFields({
                                        type: 'check',
                                        id: docNo,
                                        values: {
                                            'custbody_jvcreated': true
                                        }
                                    });
                                }
                                
                            }
                            log.emergency('JV Created',journalId)
                        }
                    }
                }
            }catch(e){
                if (e instanceof nlobjError) {
                
                log.debug("Error Code= "+e.getCode(),"Error Details= "+e.getDetails());
                
                } else {
                
                log.debug("Error Message= ",e.toString());
                
                }
            }
        }

        return {
            afterSubmit: afterSubmit
        };
});