    /**

     * @NApiVersion 2.x

     * @NScriptType ClientScript

     * @NModuleScope SameAccount

     */

    /************************************************************************************************************************************

     ** Copyright (c) 1998-2019 Softype, Inc.

     ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.

     ** All Rights Reserved.

     ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").

     ** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                  

     **                       

     ** @Author      : Farhan S

     ** @Dated       : 13/06/2019   DD/MM/YYYY

     ** @Version     : 1.0

     ** @Description : ClientScript Deployed on Transactions['Job','file'] for Setting Transaction Status Depending on Custom Record(Status Trigger List).  

     **************************************************************************************************************************************/
    define(['N/currentRecord','N/https','N/url','N/format','N/ui/message','N/search','N/runtime','N/record'],

    function(currentRecord,https,url,format,message,search,runtime,record) {
        //"mode" is used globally to get the context in which script is being triggered - create,copy,edit,...
        var mode;
        /*
         *On Page Initializaton :
         **1- Get the mode 
         **2- if 'Mode' is other than edit then directly return because we need to Enable the Fields 
              and on create & copy we should not enable any fields 
         **3- If the mode is edit , Script will get the value of the field 'custbody_fieldforscript'
              This Field is Hidden , And Script Set IDs of Fields to be enabled as values in this field in 'SaveRec' function.
              Fields IDs are comma separated, So script first split the IDs on the basis of comma.
         **4- And then Enable those fields one after other.
        */
        function pageInit(scriptContext) {
            mode = scriptContext.mode;
            //log.debug('mode', mode);
            if (mode!='edit' && mode!='copy') {
                return;
            }
            var currentRecordObj = scriptContext.currentRecord;
            var fieldsToEnable=currentRecordObj.getValue('custbody_fieldforscript');
            //log.debug('fieldsToEnable', fieldsToEnable);
            var arrayofFields=fieldsToEnable.split(',');
            //log.debug('arrayofFields', arrayofFields);
            for(var i=0;i<=arrayofFields.length-2;i++){
                //log.debug('arrayofFields Length', arrayofFields.length);
                var fieldObj = currentRecordObj.getField({
                    fieldId: arrayofFields[i]
                });
                //log.debug('fieldObj', fieldObj);
                fieldObj.isDisabled = false;
            }
        }

        function fieldChanged(scriptContext){
            var businessTypeValue,modeOfTransport,customerType,customerName;
            var currentRecordObj = currentRecord.get();
            
        }

        /*
         *Take The values from some specific fields of Transaction such as :
         *@@ Transaction Status 
         *@@ Business Type
         *@@ Mode Of Transport
         *@@ Subsidiary
         *@@ Customer Type 
         *@@ Service List
         *
         *On the basis of the value of the above fields scripts filters out 'Status Trigger List' records.
         *Then a search returns all the matching records(cases) which is stored in  'searchStatusTrigger' variable.
         *Script loads each record and get the text of two fields 1-Condition Trigger & 2- Exclude Trigger & 3- Next Trigger.
         *Condition Trigger - This field contains all the IDs of field of Transaction Record that must have values before updating the status of Transaction.
         *Exclude Trigger   - This Field contains all the IDs of field of Transaction Record that must not have values before updating the status of Transaction.
         *Next Trigger      - Once all the conditions are satisfied and Trabsaction Status is Updated, field IDs of Transaction Record presemt in this field are enabled next. 
         *There can be three cases:
         * 1- When both the fields will have values i.e. Condition Trigger & Exclude Trigger.
            * In this case script will check all the fields on Transaction Record whose IDs are present in the 'Condition Trigger' or 'Exclude Trigger'
            * If all the 'Condition Trigger' fields have values & all the 'Exclude Trigger' fields are null or 'F'(incase of checkbox)
            * Script will set status of the Transaction Record as mentioned in the 'TRANSACTION STATUS' Field of that 'Status Trigger List' record.
            * All the field IDs present in the 'Next Trigger' field will then be stored in a hidden field 'custbody_fieldforscript',
            * So that on the next pageInit all those fields are enabled. 
         * 2- When Condition Trigger field have value but Exclude Trigger field will not have any value.
            * If only 'Condition Trigger' field have values, Script will check if all the fields of Transaction Record mentioned in this field have value.
            * Script will set status of the Transaction Record as mentioned in the 'TRANSACTION STATUS' Field of that 'Status Trigger List' record.
            * All the field IDs present in the 'Next Trigger' field will then be stored in a hidden field 'custbody_fieldforscript',
            * So that on the next pageInit all those fields are enabled. 
         * 3- When Exclude Trigger field Have Value but Condition Trigger will not have any value. 
            * If only 'Exclude Trigger' field have values, Script will check if all the fields of Transaction Record mentioned in this field does not have any value.
            * Script will set status of the Transaction Record as mentioned in the 'TRANSACTION STATUS' Field of that 'Status Trigger List' record.
            * All the field IDs present in the 'Next Trigger' field will then be stored in a hidden field 'custbody_fieldforscript',
            * So that on the next pageInit all those fields are enabled. 
         *Script will check one 'Status Trigger List' record after another.
        */

        function saveRecord(scriptContext){
            log.debug('Start Time', Date());
            var currentRecordObj = scriptContext.currentRecord;
            //log.debug('mode in save record', mode);
            if (mode == 'edit' || mode == 'create' || mode == 'copy') {
                //log.emergency('inside main if');
                /* Getting the current record object*/
                var currentRecordObj = currentRecord.get();
                //log.debug('CurrRecObj', currentRecordObj);
                /* Get all the values from the Transaction record*/
                var recType = currentRecordObj.type;
                //log.debug('Rec Type', recType);
                if (recType=='invoice') {
                    recType='7'
                }else if (recType=='salesorder'){
                    recType='31'
                }
                //log.debug('Rec Type After', recType);
                /* Get the Job's Internal ID*/
                var jobId = currentRecordObj.id;
                //log.debug('Job id', jobId);
                /* Get the value of current transaction status*/
                var transactionStatus = currentRecordObj.getValue('custbody_transactionstatus');
                //log.debug('Transaction Status', transactionStatus);
                /* Get the value for Business Type*/
                var businessType = currentRecordObj.getValue('custbody_businesstype');
                //log.debug('Business Type', businessType);
                /* Get the value for Mode Of Transport*/
                var modeOfTransport = currentRecordObj.getValue('custbody_modeoftransport');
                //log.debug('Mode Of Transport', modeOfTransport);
                /* Get the value for Subsidiary*/
                var subsidiary = currentRecordObj.getValue('subsidiary');
                //log.debug('Subsidiary', subsidiary);
                /* Get the value for Customer Type*/
                var customerType = currentRecordObj.getValue('custbodycustbody_customertype');
                //log.debug('Customer Type', customerType);
                var serviceList = currentRecordObj.getValue('custbody_servicetype');
                //log.debug('Service List', serviceList);
                var triggerFilter = new Array();
                /*Filter Out the Records of Status Matrix based on the different field's value of SalesOrder(Job)*/
                /*Will use filter only if it has non-empty value*/
                if (recType!= null && recType != '' && recType != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_transactiontype',
                        operator: 'is',
                        values: recType
                    }));
                }
                    
                if (businessType!= null && businessType != '' && businessType != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_businesstype',
                        operator: 'is',
                        values: businessType
                    }));
                }
                if (modeOfTransport!= null && modeOfTransport != '' && modeOfTransport != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_modeoftransport',
                        operator: 'is',
                        values: modeOfTransport
                    }));
                }
                if (transactionStatus!= null && transactionStatus != '' && transactionStatus != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_previousstatus',
                        operator: 'is',
                        values: transactionStatus
                    }));
                }else{
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_previousstatus',
                        operator: 'anyof',
                        values:'@NONE@'
                    }));
                }
                if (subsidiary!= null && subsidiary != '' && subsidiary != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_subsidiary',
                        operator: 'is',
                        values: subsidiary
                    }));
                }
                if (businessType!= null && businessType != '' && businessType != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_customertype',
                        operator: 'is',
                        values: customerType
                    }));
                }
                if (serviceList!= null && serviceList != '' && serviceList != undefined) {
                    triggerFilter.push(search.createFilter({
                        name: 'custrecord_matrix_servicelist',
                        operator: 'allof',
                        values: serviceList
                    }));
                }
                var triggerColumns =new Array();
                /*Used internal id to load matching STATUS TRIGGER LIST custom record*/
                triggerColumns.push(search.createColumn({name:'internalid'}));
                    
                var searchStatusTrigger = search.create({
                    type: 'customrecord_statusmatrix',
                    filters: triggerFilter,
                    columns: triggerColumns
                }).run().getRange({start:0,end:1000});
                //log.debug('Search Result',searchStatusTrigger);

                if (searchStatusTrigger.length==0) {
                    return true;
                }

                for(var searchid=0;searchid<searchStatusTrigger.length;searchid++){
                    var flag=true;
                    var flagForTrigger=true;
                    var flagForExcludeTrigger=true;
                    var loadCustomRecord = record.load({
                        type : 'customrecord_statusmatrix',
                        id : searchStatusTrigger[searchid].id
                    });

                    /*var loadCustomRecord = search.lookupFields({
                        type: 'customrecord_statusmatrix',
                        id: searchStatusTrigger[searchid].id,
                        columns: ['custrecord_matrix_trigger','custrecord_matrix_excludetriggerpoints','custrecord_matrix_nexttriggerpoints']
                    });*/

                    //log.emergency('loadedRecord',loadCustomRecord);

                    var loadResultTriggerList=loadCustomRecord.getText('custrecord_matrix_trigger');
                    //log.debug('Search Result [Trigger List]',loadResultTriggerList);

                    var loadResultExcludeTriggerList=loadCustomRecord.getText('custrecord_matrix_excludetriggerpoints');
                    //log.debug('Search Result [Exclude Trigger List]',loadResultExcludeTriggerList);

                    if (loadResultTriggerList.length==0) {
                        flagForTrigger=false;
                    }

                    if (loadResultExcludeTriggerList.length==0) {
                        flagForExcludeTrigger=false;
                    }

                    if (flagForTrigger==true && flagForExcludeTrigger==true) {
                        for(var i=0;i<loadResultTriggerList.length;i++){
                            //log.debug('for loop[Trigger Field]');
                            var triggerListFieldOnTransaction=currentRecordObj.getValue(loadResultTriggerList[i]);
                            if (triggerListFieldOnTransaction == null || triggerListFieldOnTransaction == '' || triggerListFieldOnTransaction == undefined) {
                                //log.emergency('statusTriggerList Record Skipped','<EMPTY VALUE FOR "Trigger List Field">'+loadCustomRecord);
                                flag=false;
                                break;
                            }

                            if (i==loadResultTriggerList.length-1) {
                                //log.debug('All Trigger List Fields Have Value');

                                for(var j=0;j<loadResultExcludeTriggerList.length;j++){
                                    var NextTriggerListFieldOnTransaction=currentRecordObj.getValue(loadResultExcludeTriggerList[j]);
                                    if (NextTriggerListFieldOnTransaction != null && NextTriggerListFieldOnTransaction != '' && NextTriggerListFieldOnTransaction != undefined && NextTriggerListFieldOnTransaction!= 'F' ) {
                                        //log.emergency('statusTriggerList Record Skipped','<EMPTY VALUE FOR "Exclude Trigger List Field">'+loadCustomRecord);
                                        flag=false;
                                        break;
                                    }

                                    if (j==loadResultExcludeTriggerList.length-1) {
                                        //log.debug('All Next Trigger List Fields Have Value');

                                        var transStatusLoad=loadCustomRecord.getValue('custrecord_matrix_transactionstatus');

                                        var loadResultServiceList=loadCustomRecord.getValue('custrecord_matrix_servicelist');
                                        //log.debug('Search Result [Service List]',loadResultServiceList);
                                        
                                        var loadResultNextTriggerList=loadCustomRecord.getText('custrecord_matrix_nexttriggerpoints');
                                        //log.debug('Search Result [Next Trigger List]',loadResultNextTriggerList);

                                        if (flag==true) {
                                            currentRecordObj.setValue('custbody_transactionstatus',transStatusLoad);
                                            var fieldsForPageInit='';
                                            for(var m=0;m<loadResultNextTriggerList.length;m++){
                                                //log.debug('Enabling field(BEFORE)',loadResultNextTriggerList[m]);
                                                var cRObj = scriptContext.currentRecord;

                                                fieldsForPageInit+=loadResultNextTriggerList[m]+',';
                                            }
                                            currentRecordObj.setValue('custbody_fieldforscript',fieldsForPageInit);
                                            log.debug('End Time', Date());
                                            //return true;
                                        }else{
                                            //log.emergency('FLAG (false)',flag);
                                        }

                                    }
                                }
                            }
                        }
                    }else if (flagForTrigger==false && flagForExcludeTrigger==true) {
                        for(var j=0;j<loadResultExcludeTriggerList.length;j++){
                            var NextTriggerListFieldOnTransaction=currentRecordObj.getValue(loadResultExcludeTriggerList[j]);
                            if (NextTriggerListFieldOnTransaction != null && NextTriggerListFieldOnTransaction != '' && NextTriggerListFieldOnTransaction != undefined && NextTriggerListFieldOnTransaction!= 'F' ) {
                                //log.emergency('statusTriggerList Record Skipped','<EMPTY VALUE FOR "Exclude Trigger List Field">'+loadCustomRecord);
                                flag=false;
                                break;
                            }
                            if (j==loadResultExcludeTriggerList.length-1) {
                                //log.debug('All Next Trigger List Fields Have Value');
                                var transStatusLoad=loadCustomRecord.getValue('custrecord_matrix_transactionstatus');
                                var loadResultServiceList=loadCustomRecord.getValue('custrecord_matrix_servicelist');
                                //log.debug('Search Result [Service List]',loadResultServiceList);
                                var loadResultNextTriggerList=loadCustomRecord.getText('custrecord_matrix_nexttriggerpoints');
                                //log.debug('Search Result [Next Trigger List]',loadResultNextTriggerList);
                                
                                if (flag==true) {
                                    currentRecordObj.setValue('custbody_transactionstatus',transStatusLoad);
                                    var fieldsForPageInit='';
                                    for(var m=0;m<loadResultNextTriggerList.length;m++){
                                        fieldsForPageInit+=loadResultNextTriggerList[m]+',';
                                    }
                                    currentRecordObj.setValue('custbody_fieldforscript',fieldsForPageInit);
                                    log.debug('End Time', Date());
                                    //return true;
                                }else{
                                    //log.emergency('FLAG (false)',flag);
                                }
                            }
                        }
                    }else if (flagForTrigger==true && flagForExcludeTrigger==false) {
                        for(var i=0;i<loadResultTriggerList.length;i++){
                            //log.debug('for loop[Trigger Field]');
                            var triggerListFieldOnTransaction=currentRecordObj.getValue(loadResultTriggerList[i]);
                            if (triggerListFieldOnTransaction == null || triggerListFieldOnTransaction == '' || triggerListFieldOnTransaction == undefined) {
                                //log.emergency('statusTriggerList Record Skipped','<EMPTY VALUE FOR "Trigger List Field">'+loadCustomRecord);
                                flag=false;
                                break;
                            }
                            if (i==loadResultTriggerList.length-1) {
                                var transStatusLoad=loadCustomRecord.getValue('custrecord_matrix_transactionstatus');
                                var loadResultServiceList=loadCustomRecord.getValue('custrecord_matrix_servicelist');
                                //log.debug('Search Result [Service List]',loadResultServiceList);
                                var loadResultNextTriggerList=loadCustomRecord.getText('custrecord_matrix_nexttriggerpoints');
                                //log.debug('Search Result [Next Trigger List]',loadResultNextTriggerList);
                                if (flag==true) {
                                    currentRecordObj.setValue('custbody_transactionstatus',transStatusLoad);
                                    var fieldsForPageInit='';
                                    for(var m=0;m<loadResultNextTriggerList.length;m++){
                                        fieldsForPageInit+=loadResultNextTriggerList[m]+',';
                                    }
                                    currentRecordObj.setValue('custbody_fieldforscript',fieldsForPageInit);
                                    log.debug('End Time', Date());
                                    //return true;
                                }else{
                                    //log.emergency('FLAG (false)',flag);
                                }
                            }
                        }
                    }else if (flagForTrigger==false && flagForExcludeTrigger==false) {
                        var transStatusLoad=loadCustomRecord.getValue('custrecord_matrix_transactionstatus');
                        currentRecordObj.setValue('custbody_transactionstatus',transStatusLoad);

                        var loadResultNextTriggerList=loadCustomRecord.getText('custrecord_matrix_nexttriggerpoints');
                        //log.debug('Search Result [Next Trigger List]',loadResultNextTriggerList);
                        
                        var fieldsForPageInit='';
                        for(var m=0;m<loadResultNextTriggerList.length;m++){
                            fieldsForPageInit+=loadResultNextTriggerList[m]+',';
                        }
                        currentRecordObj.setValue('custbody_fieldforscript',fieldsForPageInit);
                        log.debug('End Time', Date());
                        //return true;
                    }
                }                
            }
            log.debug('End Time', Date());
            return true;    
        }

        return {
            pageInit: pageInit,
            saveRecord:saveRecord,
            fieldChanged:fieldChanged
        };
    });