/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Sep 2019     Saroja
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
var status = "2";
function DepositPrint(type, form, request){
	if(type == "view")
	{
		
		var recType = nlapiGetRecordType();
		var recId = nlapiGetRecordId();
		nlapiLogExecution('Debug', 'Record Details', 'recType '+recType+' recId '+recId);

		var action = '';
		
		action = '&action=Depositprint';
		
		if(recId != '' && recId != null &&  recType != '' && recType != null) 
		{
			nlapiLogExecution('Debug', 'Record Details2', 'recType '+recType+' recId '+recId);
			 
			var loadRec = nlapiLoadRecord(recType,recId);
			
			var url = nlapiResolveURL('SUITELET', 'customscript_softype_st_deposit_print', 'customdeploy_softype_st_deposit_print');
			nlapiLogExecution('Debug', ' URL', url);

			url += action;
			url += '&recId='+recId;
			url += '&recType='+recType;

			nlapiLogExecution('Debug', 'URL 2', url);
			var stringScript="window.open('"+url+"','_blank','toolbar=yes, location=yes, status=yes, menubar=yes, scrollbars=yes')";
			nlapiLogExecution('Debug', 'stringScript',stringScript);
			//print button is added in the form
			var customButton = form.addButton('custpage_deposit', 'Print', stringScript);
			nlapiLogExecution('Debug', 'next');
	  }
	}
//}
}