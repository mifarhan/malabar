/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 Sep 2017     Saroja
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function DepositPrint(request, response){
	
	var action = request.getParameter('action');
	nlapiLogExecution('Debug','action',action);
	if(action == 'Depositprint')
	{
		var RecId = request.getParameter('recId');
		var RecType = request.getParameter('recType');
		var loadRec = nlapiLoadRecord(RecType,RecId);
		var subsidiary = loadRec.getFieldValue('subsidiary');
		var memo = loadRec.getFieldText('memo');
		var exchangeRate = loadRec.getFieldValue('exchangerate');
		var curreny = loadRec.getFieldText('currency');
		var id = loadRec.getFieldValue('tranid');
		var date = loadRec.getFieldValue('trandate');
		//var title = loadRec.getFieldValue('title');
		var total = loadRec.getFieldValue('total');
		var preparedBy = loadRec.getFieldText('custbody_createdby');
		
		 var addressobj = nlapiLoadRecord('subsidiary', subsidiary);	
		 var address1 = addressobj.getFieldValue('addr1');	
		 var address2 = addressobj.getFieldValue('addr2');	
		 var addressee = addressobj.getFieldValue('addressee');
		 /* var city = addressobj.getFieldText('city');
		 var state = addressobj.getFieldText('state');
		 var zip = addressobj.getFieldText('zip'); */
		 var country = addressobj.getFieldText('country');	

				var image='https://5228963.app.netsuite.com/core/media/media.nl?id=1395&c=5228963&h=f94337f4f6508179777e';
				var html = '';
				html += '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">';
				html += '<pdf>';
				html += '<head>';
				html+='<macrolist>';
				html+='<macro id="nlheader">';
				html+='<table class="header" margin-bottom="10px" style="width: 100%;"><tr>';
				html+='<td rowspan="3">';
				html+='<div style="margin-bottom:0px"><img src="https://5228963.app.netsuite.com/core/media/media.nl?id=566&amp;c=5228963&amp;h=17eb39715fb5083edf6d&amp;fcts=20190521033709&amp;whence=" style="font-size: 12px; height: 42px; width: 250px;" /></div>';
				html+='</td>';
				html+='<td align="right" style="font-size:26px"><b>Cash/Staff Cheque Receipt</b></td>';
				html+='</tr>';
				html+='</table>';
				
				html+='<table margin-top="15px" align="right" border="0" cellpadding="1" cellspacing="0" style="width:100%">';
				html+='<tr>';
				html+='<td>';
				html+='<table align="left">';
				html+='<tr>';
				html+='<td align="left" style = "font-size: 12px;"><b>'+ (addressee == null? "" :addressee)+'</b></td>';
				html+='</tr>';                                     
				html+='<tr>';                                      
				html+='<td align="left" style = "font-size: 12px;"><b>'+ (address1 == null? "" :address1)+'</b></td>';
				html+='</tr>';                                     
				html+='<tr>';                                      
				html+='<td align="left" style = "font-size: 12px;"><b>'+ (address2 == null? "" :address2)+'</b></td>';
				html+='</tr>';                                     
				html+= '<tr>';                                     
				html+= '<td align="left" style = "font-size: 12px;"><b>'+ (country == null? "" :country)+'</b></td>';
				html+= '</tr>';
				html+='</table>'; 
				html+='</td>';
				
				html+='<td>';
				html+='<table align="right" border="1">';
				html+='<tr>';
				html+='<td border-right="1px" border-bottom="1px"  style="font-size: 12px;">ID</td>';
				html+='<td border-bottom="1px" style="font-size:12px" align="right">'+(id == null? "" :id)+'</td>';
				html+='</tr>';
				html+='<tr>';
				html+='<td border-right="1px" border-bottom="1px" style="font-size: 12px;">Date</td>';
				html+='<td border-bottom="1px" style="font-size:12px" align="right">'+(date == null? "" :date)+'</td>';
				html+='</tr>';
				html+='<tr>';
				html+='<td border-right="1px" border-bottom="1px" style="font-size:12px">Currency</td>';
				html+='<td border-bottom="1px" style="font-size:12px" align="right">'+(curreny == null? "" :curreny)+'</td>';
				html+='</tr>';
				html+='<tr>';
				html+='<td border-right="1px" style="font-size:12px" >Exchange Rate</td>';
				html+='<td style="font-size:12px" align="right">'+(exchangeRate == null? "" :exchangeRate)+'</td>';
				html+='</tr>';
				/* html+='<tr>';
				html+='<td border-right="1px" style="font-size:12px" >Memo</td>';
				html+='<td style="font-size:12px" align="right">'+memo+'</td>';
				html+='</tr>'; */
				html+='</table>';
				html+='</td>';
				html+='</tr>';
				html+='</table>'; 
				
				// html+='<table align="left" class="header" style="width: 100%; margin-top: 10px;margin-bottom: 10px;"><tr>';
				// html+='<td style="font-size:12px"><b>Received From : </b></td>';
				// html+='</tr>';
				// /* html+='<tr>';
				// html+='<td style="font-size:12px">${record.customer.address}</td>'; 
				// html+='</tr>'; */
				// html+='</table>';
				html+='</macro>';
				html+='</macrolist>';

				html+= '<style type="text/css">* {';
				html+= 'table {';
				html+= 'font-size: 9pt;';
				html+= 'table-layout: fixed;';
				html+= '}';
				html+= 'th {';
				html+= 'font-weight: bold;';
				html+= 'font-size: 8pt;';
				html+= 'vertical-align: middle;';
				html+= 'padding: 5px 6px 3px;';
				html+= 'background-color: #e3e3e3;  ';
				html+= 'color: #333333;';
				html+= '}';
				html+= 'td { ';
				html+= 'padding: 4px 6px;';
				html+= '}';
				html+= 'td p { align:left }';
				html+= 'b { ';
				html+= 'font-weight: bold;';
				html+= 'color: #333333;';
				html+= '}';
				html+= 'table.header td {';
				html+= 'padding: 0;';
				html+= 'font-size: 10pt;';
				html+= '}';
				html+= 'table.footer td {';
				html+= 'padding: 0;';
				html+= 'font-size: 8pt;';
				html+= '}';
				html+= 'table.itemtable th {';
				html+= 'padding-bottom: 10px;';
				html+= 'padding-top: 10px;';
				html+= '}';
				html+= 'table.body td {';
				html+= 'padding-top: 2px;';
				html+= '}';
				html+= 'table.total {';
				html+= 'page-break-inside: avoid;';
				html+= '}';
				html+= 'tr.totalrow {';
				html+= 'background-color: #e3e3e3;';
				html+= 'line-height: 200%;';
				html+= '}';
				html+= 'td.totalboxtop { ';
				html+= 'font-size: 12pt; ';
				html+= 'background-color: #e3e3e3;';
				html+= '} ';
				html+= 'td.addressheader {';
				html+= 'font-size: 8pt;';
				html+= 'padding-top: 6px;';
				html+= 'padding-bottom: 2px;';
				html+= '}';
				html+= 'td.address {';
				html+= 'padding-top: 0;';
				html+= '} ';
				html+= 'td.totalboxmid {';
				html+= 'font-size: 28pt;';
				html+= 'padding-top: 20px;';
				html+= 'background-color: #e3e3e3;';
				html+= '}';
				html+= 'span.title { ';
				html+= 'font-size: 28pt;';
				html+= '}';
				html+= 'span.number {';
				html+= 'font-size: 16pt;';
				html+= '}';
				html+= 'hr {';
				html+= 'width: 100%;';
				html+= 'color: #d3d3d3;';
				html+= 'background-color: #d3d3d3;';
				html+= 'height: 1px;';
				html+= '}';
				html+= '</style>';
				html += '</head>';

				html += '<body header ="nlheader" header-height="18%" padding="0.1in 0.5in 0.5in 0.5in" size="Letter">';

				html+='<table align="left" border-right="1px" border-left="1px" border-top="1px" style="width:100%;">';
				html+='<thead>';
				html+='	<tr>';
				html+='	<th border-bottom="1px" border-right="1px" align="center" style="width:200px;background-color: #e3e3e3;font-size:12px"><b>Name</b></th>';
				html+='	<th border-bottom="1px" border-right="1px" align="center" style="width:80px;background-color: #e3e3e3;font-size:12px"><b>Account</b></th>';
				html+='	<th border-bottom="1px" border-right="1px" align="center" style="width:60px;background-color: #e3e3e3;font-size:12px"><b>Payment Method</b></th>';
				html+='	<th border-bottom="1px" border-right="1px" align="center" style="width:200px;background-color: #e3e3e3;font-size:12px;"><b>Narration</b></th>';
				html+='	<th border-bottom="1px" align="center" style="width:80px;background-color: #e3e3e3;font-size:12px"><b>Amount</b></th>';
				html+='	</tr>';
				html+='	</thead>';
				
				var lineCount = loadRec.getLineItemCount('other');
				if(lineCount > 0)
				{
					for(var i = 1; i <= lineCount; i++)
					{
						var name = loadRec.getLineItemValue('other','entity_display',i);
						var account = loadRec.getLineItemValue('other','account_display',i);
						var paymentmethod = loadRec.getLineItemText('other','paymentmethod',i);
						var amount = loadRec.getLineItemValue('other','amount',i);
						var memo = loadRec.getLineItemValue('other','memo',i);
						html+='	<tr>';
						html+='	<td border-bottom="1px" padding-left="5px" border-right="1px" style="width:200px;font-size:12px">'+(name == null? "" :name)+'</td>';
						html+='	<td border-bottom="1px" padding-left="5px" border-right="1px" style="width:80px;font-size:12px">'+(account == null? "" :account)+'</td>';
						html+='	<td border-bottom="1px" padding-left="5px" border-right="1px" style="width:60px;font-size:12px">'+(paymentmethod == null? "" :paymentmethod)+'</td>';
						
						html+='	<td border-bottom="1px" padding-left="5px" align="right" style="border-right:1px;width:200px;font-size:12px">'+(memo == null? "" :memo)+'</td>';
						html+='	<td border-bottom="1px" padding-right="5px" align="right" style="width:80px;font-size:12px">'+ (amount == null? "" :amount)+ '</td>';
						html+='	</tr>';     
		
					}
				}
				html+='</table>';
				html+='<table align="right" class="total" style="width: 100%; margin-top: 10px;"><tr class="totalrow">';
				html+='	<td background-color="#ffffff" colspan="7">&nbsp;</td>';
				html+='	<td background-color="#e3e3e3" align="left"><b>Total:</b></td>';
				html+='	<td background-color="#e3e3e3" align="right">'+total+'</td>';
				html+='	</tr></table> '; 
				html+='<table margin-top="20px" style="width: 100%;"><tr>';
				html+='<td colspan="3" align="left" style="padding-left: 0px;font-size: 12px;margin-bottom:25px;"><b>Prepared by:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+(preparedBy == null? "" :preparedBy)+'&nbsp;&nbsp;_______________________</td>';
				html+='</tr>';
				html+='<tr>';
				html+='<td align="left" style="padding-left: 0px;font-size: 12px;"><b>Authorized by:</b>&nbsp;&nbsp;_______________________</td>';
				html+='<td align="center" style="padding-left: 0px;font-size: 12px;"><b>Received by:</b>&nbsp;&nbsp; ______________________</td>';
				html+='<td align="right" style="font-size: 12px;"><b>Signature:</b>&nbsp;&nbsp; ______________________</td>';
				html+='</tr></table>';				
				html += '</body>';
				html += '</pdf>'; 

				nlapiLogExecution('Debug', 'HTML Layout', html);

				var file = nlapiXMLToPDF(html);
				response.setContentType('PDF','Print.pdf ','inline');
				response.write(file.getValue());

		}

}





