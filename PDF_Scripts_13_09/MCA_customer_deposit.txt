<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">
<pdf>
<head>
	<link name="NotoSans" type="font" subtype="truetype" src="${nsfont.NotoSans_Regular}" src-bold="${nsfont.NotoSans_Bold}" src-italic="${nsfont.NotoSans_Italic}" src-bolditalic="${nsfont.NotoSans_BoldItalic}" bytes="2" />
	<#if .locale == "zh_CN">
		<link name="NotoSansCJKsc" type="font" subtype="opentype" src="${nsfont.NotoSansCJKsc_Regular}" src-bold="${nsfont.NotoSansCJKsc_Bold}" bytes="2" />
	<#elseif .locale == "zh_TW">
		<link name="NotoSansCJKtc" type="font" subtype="opentype" src="${nsfont.NotoSansCJKtc_Regular}" src-bold="${nsfont.NotoSansCJKtc_Bold}" bytes="2" />
	<#elseif .locale == "ja_JP">
		<link name="NotoSansCJKjp" type="font" subtype="opentype" src="${nsfont.NotoSansCJKjp_Regular}" src-bold="${nsfont.NotoSansCJKjp_Bold}" bytes="2" />
	<#elseif .locale == "ko_KR">
		<link name="NotoSansCJKkr" type="font" subtype="opentype" src="${nsfont.NotoSansCJKkr_Regular}" src-bold="${nsfont.NotoSansCJKkr_Bold}" bytes="2" />
	<#elseif .locale == "th_TH">
		<link name="NotoSansThai" type="font" subtype="opentype" src="${nsfont.NotoSansThai_Regular}" src-bold="${nsfont.NotoSansThai_Bold}" bytes="2" />
	</#if>
    <macrolist>
        <macro id="nlheader">
			<table class="header" margin-bottom="10px" style="width: 100%;"><tr>
              <td>
              <table align="left">
                <tr>
                  <td rowspan="3">
                <div style="margin-bottom:0px"><img src="https://5228963.app.netsuite.com/core/media/media.nl?id=566&amp;c=5228963&amp;h=17eb39715fb5083edf6d&amp;fcts=20190521033709&amp;whence=" style="font-size: 12px; height: 42px; width: 250px;" /></div>
                  </td>

                </tr>
              </table>
              </td>
              <td>
                <table align="right">
                <tr><td align="right" margin-bottom="0px" style="font-size: 26px;">Client Cash/Cheque Receipt</td></tr>
                </table>
              </td>
          </tr></table>

			<table width="100%">
              <tr>
                <td><b>${subsidiary.mainaddress_text}</b><br/><br/><br/><b>Received From :</b><br/><br/>${record.customer.address}</td>
				<td>
                <table margin-top="0px" align="right" border="1" cellpadding="1" cellspacing="0" style="width:250px;">
                    <tr>
                    <td border-right="1px" border-bottom="1px" style="padding-top:10px;font-size: 12px;">${record.tranid@label}</td>
                      <td border-bottom="1px" style="fone-size:12px" align="right">${record.tranid}</td>
                    </tr>
                    <tr>
                    <td border-right="1px" border-bottom="1px" style="padding-top:10px;font-size: 12px;">${record.trandate@label}</td>
                      <td border-bottom="1px" style="fone-size:12px" align="right">${record.trandate}</td>
                    </tr>
                    <tr>
                    <td border-right="1px" border-bottom="1px" style="fone-size:12px">${record.currency@label}</td>
                    <td border-bottom="1px" style="fone-size:12px" align="right">${record.currency}</td>
                    </tr>
                    <tr>
                    <td border-right="1px" border-bottom="1px" style="fone-size:12px" >${record.exchangerate@label}</td>
                    <td border-bottom="1px" style="fone-size:12px" align="right">${record.exchangerate}</td>
                    </tr>
                    <tr>
                    <td border-right="1px" border-bottom="1px" style="fone-size:12px" >${record.paymentmethod@label}</td>
                    <td border-bottom="1px" style="fone-size:12px" align="right">${record.paymentmethod}</td>
                    </tr>
                    <tr>
                    <td border-right="1px" style="fone-size:12px">Check No.</td>
                    <td style="fone-size:12px" align="right">${record.checknum}</td>
                    </tr>
                  </table></td></tr>
          </table>

<!--table align="left" class="header" style="width: 100%; margin-top: 10px;margin-bottom: 10px;"><tr>
	<td style="fone-size:12px"><b>Received From :</b></td>
	</tr>
	<tr>
	<td style="fone-size:12px">${record.customer.address}</td>
	</tr></table-->
        </macro>
    </macrolist>
    <style type="text/css">* {
		<#if .locale == "zh_CN">
			font-family: NotoSans, NotoSansCJKsc, sans-serif;
		<#elseif .locale == "zh_TW">
			font-family: NotoSans, NotoSansCJKtc, sans-serif;
		<#elseif .locale == "ja_JP">
			font-family: NotoSans, NotoSansCJKjp, sans-serif;
		<#elseif .locale == "ko_KR">
			font-family: NotoSans, NotoSansCJKkr, sans-serif;
		<#elseif .locale == "th_TH">
			font-family: NotoSans, NotoSansThai, sans-serif;
		<#else>
			font-family: NotoSans, sans-serif;
		</#if>
		}
		table {
			font-size: 9pt;
			table-layout: fixed;
		}
        th {
            font-weight: bold;
            font-size: 8pt;
            vertical-align: middle;
            padding: 5px 6px 3px;
            background-color: #e3e3e3;
            color: #333333;
        }
        td {
            padding: 4px 6px;
        }
		td p { align:left }
        b {
            font-weight: bold;
            color: #333333;
        }
        table.header td {
            padding: 0;
            font-size: 10pt;
        }
        table.footer td {
            padding: 0;
            font-size: 8pt;
        }
        table.itemtable th {
            padding-bottom: 10px;
            padding-top: 10px;
        }
        table.body td {
            padding-top: 2px;
        }
        table.total {
            page-break-inside: avoid;
        }
        tr.totalrow {
            background-color: #e3e3e3;
            line-height: 200%;
        }
        td.totalboxtop {
            font-size: 12pt;
            background-color: #e3e3e3;
        }
        td.addressheader {
            font-size: 8pt;
            padding-top: 6px;
            padding-bottom: 2px;
        }
        td.address {
            padding-top: 0;
        }
        td.totalboxmid {
            font-size: 28pt;
            padding-top: 20px;
            background-color: #e3e3e3;
        }
        span.title {
            font-size: 28pt;
        }
        span.number {
            font-size: 16pt;
        }
        hr {
            width: 100%;
            color: #d3d3d3;
            background-color: #d3d3d3;
            height: 1px;
        }
</style>
</head>
<body header="nlheader" header-height="22%" padding="0.1in 0.5in 0.5in 0.5in" size="Letter">
    <table align="left" border="1" style="width:100%;">
<thead>
	<tr>
	<th border-bottom="1px" align="center" border-right="1px" style="width:495px;fone-size:12px">Bank/Cash</th>
	<th border-bottom="1px" align="center" style="width:80px;fone-size:12px">Amount</th>
	</tr>
</thead>
     <tr>
	<td padding-left="5px" border-right="1px" style="width:300px;fone-size:12px">${record.account}</td>
	<td padding-right="5px" align="right" style="width:80px;fone-size:12px">${record.payment}</td>
	</tr>
	<tr>
	<td style="fone-size:12px" border-right="1px">${record.memo}</td>
	<td style="fone-size:12px" align="right">&nbsp;</td>
	</tr></table>

<table align="right" class="total" style="width: 100%; margin-top: 10px;"><tr class="totalrow">
	<td background-color="#ffffff" colspan="4">&nbsp;</td>
	<td align="left"><b>Total:</b></td>
	<td align="right">${record.payment}</td>
	</tr></table>
  
  <table margin-top="10px" style="width: 100%;"><tr>
	<td colspan="3" align="left" style="padding-left: 0px;font-size: 12px;margin-bottom:35px;"><b>Prepared by:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${record.custbody_createdby}&nbsp;&nbsp;_______________________</td>
              </tr>
	<tr>
	<td align="left" style="padding-left: 0px;font-size: 12px;"><b>Authorized by:</b>&nbsp;&nbsp;_______________________</td>
	<td align="center" style="padding-left: 0px;font-size: 12px;"><b>Received by:</b>&nbsp;&nbsp; ______________________</td>
	<td align="right" style="font-size: 12px;"><b>Signature:</b>&nbsp;&nbsp; ______________________</td>
	</tr></table>
</body>
</pdf>